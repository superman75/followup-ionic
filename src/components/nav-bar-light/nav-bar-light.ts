import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the NavBarLightComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nav-bar-light',
  templateUrl: 'nav-bar-light.html'
})
export class NavBarLightComponent {

  text: string;

  constructor(public navCtrl:NavController) {
    console.log('Hello NavBarLightComponent Component');
    this.text = 'Hello World';
  }

  openChat(){
    this.navCtrl.push('MessagePage');
    }
goBack(){
    this.navCtrl.pop();
  }
}
