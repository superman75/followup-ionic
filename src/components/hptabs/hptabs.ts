import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Nav } from 'ionic-angular/navigation/nav-interfaces';

/**
 * Generated class for the HptabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hptabs',
  templateUrl: 'hptabs.html'
})
export class HptabsComponent {

  text: string;
  selectedTab :string;
  tabPassed: any ;

  constructor(public navCtrl: NavController,public navParams:NavParams) {
    console.log('Hello HptabsComponent Component');
    this.text = 'Hello World';
    //console.log(this.tabd);
    this.tabPassed = this.navParams.get('selectedTab');
    console.log(this.tabPassed);
    if (this.tabPassed) {
      this.selectedTab=this.tabPassed;
    }else{
      this.selectedTab="patient";
    }
   // this.selectedTab="patient";
  }
  selectPatient() {
    if (this.selectedTab!=="patient") {
      console.log('Click patient!');
      this.selectedTab="patient";
      this.navCtrl.push('HpDashboardPage',{selectedTab:this.selectedTab});
    }
    
    
  }
  selectSchedule(){
    if (this.selectedTab!=="schedule") {
    console.log('Click selectSchedule!');
    this.selectedTab="schedule";
    this.navCtrl.push('DctrSchdlDetailPage',{selectedTab:this.selectedTab});
    }
  }

    openChat(){
      this.navCtrl.push('DctrSchdlDetailPage',{selectedTab:this.selectedTab});
      }
    
  selectAbout(){
    if (this.selectedTab!=="about") {
    console.log('Click selectAbout!');
    this.selectedTab="about";
    this.navCtrl.push('AboutPage',{selectedTab:this.selectedTab});
    }
  }
}
