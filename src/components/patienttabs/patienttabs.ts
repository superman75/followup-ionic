import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';  

/**
 * Generated class for the PatienttabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'patienttabs',
  templateUrl: 'patienttabs.html'
})
export class PatienttabsComponent {

  text: string;
  selectedTab :string;
  tabPassed: any ;

  constructor(public navCtrl: NavController,public navParams:NavParams) {
    console.log('Hello PatienttabsComponent Component');
    this.text = 'Hello World';
    this.tabPassed = this.navParams.get('selectedTab');
    console.log(this.tabPassed);
    if (this.tabPassed) {
      this.selectedTab=this.tabPassed;
    }else{
      this.selectedTab="dashbaord";
    }
  }
  selectDashboard()
  {
    //this.navCtrl.push('PatientDashboardPage');

    if (this.selectedTab!=="dashbaord") {
      this.selectedTab="dashbaord";
      this.navCtrl.push('PatientDashboardPage',{selectedTab:this.selectedTab});
    }

  }

  selectResult()
  {
    //this.navCtrl.push('PatientDashboardPage');

    if (this.selectedTab!=="result") {
      this.selectedTab="result";
      this.navCtrl.push('PatientresultPage',{selectedTab:this.selectedTab});
    }
  }

  selectSchedule()
  {
    //this.navCtrl.push('PatientSchedulePage');
    if (this.selectedTab!=="schedule") {
      this.selectedTab="schedule";
      this.navCtrl.push('PatientSchedulePage',{selectedTab:this.selectedTab});
    }
  }

  selectAbout()
  {
   // this.navCtrl.push('PatientDashboardPage');
    if (this.selectedTab!=="about") {
      this.selectedTab="about";
      this.navCtrl.push('AboutPage',{selectedTab:this.selectedTab});
    }
  }


}
