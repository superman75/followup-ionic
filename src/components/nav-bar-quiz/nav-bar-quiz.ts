import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/**
 * Generated class for the NavBarQuizComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nav-bar-quiz',
  templateUrl: 'nav-bar-quiz.html'
})
export class NavBarQuizComponent {

  text: string;

  constructor(private navCtrl:NavController) {
    console.log('Hello NavBarQuizComponent Component');
    this.text = 'Hello World';
  }
  goBack(){
    this.navCtrl.pop();
  }
}
