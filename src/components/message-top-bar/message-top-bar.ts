import { Component } from '@angular/core';

/**
 * Generated class for the MessageTopBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'message-top-bar',
  templateUrl: 'message-top-bar.html'
})
export class MessageTopBarComponent {

  text: string;

  constructor() {
    console.log('Hello MessageTopBarComponent Component');
    this.text = 'Hello World';
  }

}
