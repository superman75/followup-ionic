import { NgModule } from '@angular/core';
import { TopNavbarComponent } from './top-navbar/top-navbar';
import { HptabsComponent } from './hptabs/hptabs';
import { NavBarLightComponent } from './nav-bar-light/nav-bar-light';
import { NavBarQuizComponent } from './nav-bar-quiz/nav-bar-quiz';
import { IonicModule } from 'ionic-angular';
import { PatienttabsComponent } from './patienttabs/patienttabs';
import { HpDetailComponent } from './hp-detail/hp-detail';
import { BackButtonComponent } from './back-button/back-button';
import { MessageTopBarComponent } from './message-top-bar/message-top-bar';

@NgModule({
	declarations: [TopNavbarComponent,
    HptabsComponent,
    NavBarLightComponent,
    NavBarQuizComponent,
    PatienttabsComponent,
    HpDetailComponent,
    BackButtonComponent,
    MessageTopBarComponent],
	imports: [IonicModule],
	exports: [TopNavbarComponent,
    HptabsComponent,
    NavBarLightComponent,
    NavBarQuizComponent,
    PatienttabsComponent,
    HpDetailComponent,
    BackButtonComponent,
    MessageTopBarComponent]
})
export class ComponentsModule {}



