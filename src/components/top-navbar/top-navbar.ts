import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the TopNavbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'top-navbar',
  templateUrl: 'top-navbar.html'
})
export class TopNavbarComponent {

  text: string;

  constructor(public navCtrl: NavController,public menuCtrl: MenuController,public modalCtrl : ModalController) {
    console.log('Hello TopNavbarComponent Component');
    this.text = 'Hello World';
  }

  openChat(){
    this.navCtrl.push('MessagePage');
    }
    search(){
      // var modalPage = this.modalCtrl.create('SearchPage'); 
      // modalPage.present(); 
      this.navCtrl.push('SearchPage');
    }
}
