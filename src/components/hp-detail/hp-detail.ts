import { Component } from '@angular/core';

/**
 * Generated class for the HpDetailComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hp-detail',
  templateUrl: 'hp-detail.html'
})
export class HpDetailComponent {

  text: string;

  constructor() {
    console.log('Hello HpDetailComponent Component');
    this.text = 'Hello World';
  }

}
