import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular';
import{SessionManagerProvider} from '../providers/session-manager/session-manager';
import { ModalController } from 'ionic-angular';
import { FCM } from '@ionic-native/fcm';
import { Dialogs } from '@ionic-native/dialogs';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { concat } from 'rxjs/observable/concat';
import {ServiceHelperProvider} from '../providers/service-helper/service-helper';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  @ViewChild('content') nav: NavController
  //set rootpage of aplication
  rootPage:any = LoginPage;
  isPatient:boolean=true;
  userName:string="";
  
  constructor(private dialogs: Dialogs,private fcm: FCM,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public menuCtrl: MenuController,public sessionManagerProvider:SessionManagerProvider,public modalCtrl:ModalController,public serviceProvider: ServiceHelperProvider) {
    this.userName=sessionManagerProvider.getName();
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
     // statusBar.styleDefault();
     statusBar.styleBlackOpaque();
     //statusBar.styleBlackOpaque();
      splashScreen.hide();
      
     // statusBar.overlaysWebView(false);
      //this.InitialiseFCMNotification();
    });
    
  }
  InitialiseFCMNotification()
  {
    this.fcm.onNotification().subscribe(data => {
     /* if(data.wasTapped){
        
      } else {
        
      };*/
      this.dialogs.alert(data.Body,data.Title,"Close")
      .then(() => console.log('Dialog dismissed'))
      .catch(e => console.log('Error displaying dialog', e));
    });
  }
  addHealthProvider(){
    var modalPage = this.modalCtrl.create('AddHealthcarePage'); 
    modalPage.present(); 
  }
  myPersonalCode(){
    var modalPage = this.modalCtrl.create('ChangeHpCodePage'); 
    modalPage.present(); 
  }
  openSettings(){
   // var modalPage = this.modalCtrl.create('SettingsPage'); 
    // modalPage.present(); 
    this.nav.push(SettingsPage); 
  }

logOut(){
 
//  localStorage.removeItem('ust');
//  localStorage.removeItem('userImage');
//  localStorage.removeItem('isPatient');
//  localStorage.removeItem('userGuid');
 this.sessionManagerProvider.clearSessionVariables();
 this.nav.push(LoginPage); 
}
menuOpened(){
  this.isPatient=this.sessionManagerProvider.getPatient();
  this.userName=this.sessionManagerProvider.getName();
  console.log(this.userName);
  
}
menuClosed(){
 // this.isPatient=this.sessionManagerProvider.getPatient();
}

}

