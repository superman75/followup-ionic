import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { HttpClientModule } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { LoadedproviderProvider } from '../providers/loadedprovider/loadedprovider';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { ToastProvider } from '../providers/toast/toast';
import { ServiceHelperProvider } from '../providers/service-helper/service-helper';
import { ChatServiceProvider } from '../providers/chat-service/chat-service';
import { EmojiProvider } from '../providers/emoji/emoji';
import { SessionManagerProvider } from '../providers/session-manager/session-manager';
import { HttpModule } from '@angular/http';

import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { Camera } from '@ionic-native/camera';
import { FCM } from '@ionic-native/fcm';
import { Dialogs } from '@ionic-native/dialogs';
import { CalendarModule } from "ion2-calendar";

// import { AboutPage } from '../pages/about/about';
// import { AddpatientPage } from '../pages/addpatient/addpatient';
// import { AssessmentsPage } from '../pages/assessments/assessments';
// import { CreateAssignmentPage } from '../pages/create-assignment/create-assignment';
// import { DctrSchdlDetailPage } from '../pages/dctr-schdl-detail/dctr-schdl-detail';
// import { ExistngPatientPage } from '../pages/existng-patient/existng-patient';
// import { HpDashboardPage } from '../pages/hp-dashboard/hp-dashboard';
// import { PatientDashboardPage } from '../pages/patient-dashboard/patient-dashboard';
// import { SignUpPage } from '../pages/sign-up/sign-up';
// import { SrvcAgrmntPage } from '../pages/srvc-agrmnt/srvc-agrmnt';
// import { StartQuizPage } from '../pages/start-quiz/start-quiz';
// import { SubmitQuizPage } from '../pages/submit-quiz/submit-quiz';
// import { ViewassessmentsdetailPage } from '../pages/viewassessmentsdetail/viewassessmentsdetail';

// import { LoginPageModule } from '../pages/login/login.module';
// import { AboutPageModule } from '../pages/about/about.module';
// import { AddpatientPageModule } from '../pages/addpatient/addpatient.module';
// import { AssessmentsPageModule } from '../pages/assessments/assessments.module';
// import { CreateAssignmentPageModule } from '../pages/create-assignment/create-assignment.module';
// import { DctrSchdlDetailPageModule } from '../pages/dctr-schdl-detail/dctr-schdl-detail.module';
// import { ExistngPatientPageModule } from '../pages/existng-patient/existng-patient.module';
// import { HpDashboardPageModule } from '../pages/hp-dashboard/hp-dashboard.module';
// import { PatientDashboardPageModule } from '../pages/patient-dashboard/patient-dashboard.module';
// import { SignUpPageModule } from '../pages/sign-up/sign-up.module';
// import { SrvcAgrmntPageModule } from '../pages/srvc-agrmnt/srvc-agrmnt.module';
// import { StartQuizPageModule } from '../pages/start-quiz/start-quiz.module';
// import { SubmitQuizPageModule } from '../pages/submit-quiz/submit-quiz.module';
// import { ViewassessmentsdetailPageModule } from '../pages/viewassessmentsdetail/viewassessmentsdetail.module';






@NgModule({
  declarations: [
    MyApp,
    HomePage,LoginPage,SettingsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  HttpModule,
  CalendarModule,
    // LoginPageModule,
    // AboutPageModule,
    // AddpatientPageModule,
    // AssessmentsPageModule,
    // CreateAssignmentPageModule,
    // DctrSchdlDetailPageModule,
    // ExistngPatientPageModule,
    // HpDashboardPageModule,PatientDashboardPageModule,SignUpPageModule,SrvcAgrmntPageModule,StartQuizPageModule,SubmitQuizPageModule,ViewassessmentsdetailPageModule,
    //set theme as material design for all componets on all platforms
    IonicModule.forRoot(MyApp,{
      mode: 'md'
  }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,LoginPage,SettingsPage
  ],
  providers: [
    ImagePicker,
		Crop,
    Camera,
    FCM,
    Dialogs,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceHelperProvider,
    LoadedproviderProvider,
    ToastProvider,
    AuthenticationProvider,
    ServiceHelperProvider,
    ChatServiceProvider,
    EmojiProvider,
    SessionManagerProvider

  ]
})
export class AppModule {}


