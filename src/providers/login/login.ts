import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoadedproviderProvider } from '../loadedprovider/loadedprovider';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {
  token: string;

  //apiUrl = 'http://portalservice.southeastasia.cloudapp.azure.com/PortalWeb/api/security/useraccount/SignIn';
  //apiUrl = 'http://localhost/followupservices/api/security/useraccount/SignIn';
  //apiUrl = 'http://ssingh-lt/followupservices/api/security/useraccount/SignIn';
  //apiUrl = 'http://follow.southeastasia.cloudapp.azure.com/followup/';
  // apiUrl: string = 'http://relevant.southeastasia.cloudapp.azure.com/followup/';
  apiUrl: string = 'http://followupservices.us-east-2.elasticbeanstalk.com/';

  constructor(public http: HttpClient, private loader: LoadedproviderProvider) {
    console.log('Hello LoginProvider Provider');
  }
  get(url) {
    this.loader.show();
    return this.http.get(url)
      .map((resp: Response) => resp.json())
      .finally(() => {
        this.loader.hide();
      });
  }

  post(url, body) {
    this.loader.show();
    return this.http.post(url, body)
      .map((resp: Response) => resp.json())
      .finally(() => {
        this.loader.hide();
      });
  }

  ////sample code for get
  getRequest(url) {
    this.loader.show();
    return new Promise(resolve => {
      this.http.get(url).subscribe(data => {
        this.loader.hide();
        resolve(data);
      }, err => {
        this.loader.hide();
        console.log(err);

      });
    });
  }

  PostRequest(url, data) {
    this.loader.show();
    // let headers = new Headers();
    // headers.append('app-id', '');
    // headers.append('app-key', '');
    // headers.append('Content-Type', 'application/json');
    return new Promise((resolve, reject) => {
      this.http.post(url, data)
        .subscribe(res => {
          this.loader.hide();
          resolve(res);
        }, (err) => {
          this.loader.hide();
          reject(err);
        });
    });
  }





}
