import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http,Headers,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoadedproviderProvider } from '../loadedprovider/loadedprovider';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {
  token :string;
  


  // apiBaseUrl:string = 'http://relevant.southeastasia.cloudapp.azure.com/followup/';
  apiBaseUrl:string = 'http://localhost/followupservices/';
  


  constructor(public http: HttpClient,private loader: LoadedproviderProvider) {
    console.log('Hello LoginProvider Provider');
  }
  get(url) {
    const headers = new HttpHeaders()
    .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
    this.loader.show();
    return this.http.get(this.apiBaseUrl + url,{headers})
      .map((resp: Response) => resp.json())
      .finally(() => {
        this.loader.hide();
      });
  }

  post(url, body) {
    const headers = new HttpHeaders()
    .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
    this.loader.show();
    return this.http.post( this.apiBaseUrl + url, body,{headers})
      .map((resp: Response) => resp.json())
      .finally(() => {
        this.loader.hide();
      });
  }

  ////sample code for get
  getRequest(url) {
    const headers = new HttpHeaders()
    .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
    this.loader.show();
    return new Promise(resolve => {
      this.http.get(this.apiBaseUrl + url,{headers}).subscribe(data => {
        this.loader.hide();
        resolve(data);
      }, err => {
        this.loader.hide();
        console.log(err);
        
      });
    });
  }

  PostRequest(url,data) {
    const headers = new HttpHeaders()
    .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
    this.loader.show();
    // let headers = new Headers();
    // headers.append('app-id', '');
    // headers.append('app-key', '');
    // headers.append('Content-Type', 'application/json');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiBaseUrl + url ,data,{headers})
        .subscribe(res => {
          this.loader.hide();
          resolve(res);
        }, (err) => {
          this.loader.hide();
          reject(err);
        });
    });
  }

  

  

}
