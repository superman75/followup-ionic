import { DateTime } from "ionic-angular";


export class PatientResult
    {
      AssessmentGuid : string;
      PatientGuid: string;
      Name: string;
      Email:string;
      Age: number;
      Category: string;
      UserGender : Gender;
      CompletedDate: DateTime;
      Result: string;
      AssessmentTitle: string;
      IsActive : boolean;
      InactiveDate: boolean;
      Diagnosis: string;
      Notes: string;
      CompletedAssessmentsCount: number;
      TotalAssessmentCount: number;
      PatientIntId: number;
      //ShowResult:boolean=false;
    }


    export enum Gender {
      Male = 1,
      Female = 2
}
export class PatientResultView extends PatientResult
{

  ShowResult:boolean=false;
}
export class User
{
  access_token: string;
  expires_in: number;
  token_type: string;
  userName: string
  AgreementFlg: string;
  IsTempPassword:string;
  Name:string;
  UserImage:string;
  UserGuid:string;
}

export class Answer
{
  constructor(){ }

  Text: string;
  Code: string;
  Score: number;
}
export class AnswerValue extends Answer
{
  Checked: boolean;
}
export class QuestionAnswer
{
  constructor()
  {
    this.Answers = [new AnswerValue()];
  }
			QuestionGuid: string;
			Question: string;
			OperationType: number;
      AnswersLookup: AnswerValue[];
      Answers: Answer[];
      AnswerString: String[];
}

export class ScoreRange{
  constructor(){ }

				MinScore: number;
				MaxScore: number;
				ResultText: string;
}

export class Assessment
{

  constructor(){ }

	AssessmentGuid: string;
	Title: string;
	CategoryCd: string;
	SubCategoryCd: string;
	UserGuid: string;
	UserIntId: number;
	OperationType: number;
	Details: string;
	Quiz: QuestionAnswer[];
	ResultRange: ScoreRange[];
	PatientGuids: string[];
}



let AnswersLookup: Answer[] = [
{Text: 'As much as I always could',Code: 'AMAIAC',Score: 0},
{Text: 'Not quite so much now',Code: 'NQSMN',Score: 0},
{Text: 'Definitely not so much now',Code: 'DNSMN',Score: 0},
{Text: 'Not at all',Code: 'NAA',Score: 0}
];



export class LookupData {
  Key: string;
  Value: string;
}

let AnswersLookupNew: AnswerValue[] = [
  {Checked:false, Text: 'As much as I always could',Code: 'AMAIAC',Score: 0},
  {Checked:false, Text: 'Not quite so much now',Code: 'NQSMN',Score: 0},
  {Checked:false, Text: 'Definitely not so much now',Code: 'DNSMN',Score: 0},
  {Checked:false, Text: 'Not at all',Code: 'NAA',Score: 0}
  ];


export class Category extends LookupData {
  SubCategories: LookupData[];
}

let CategoryList: Category[] = [
  {Key :"CRDOLGY", Value: "Cardiology", SubCategories: [ {Key :"STRSTST", Value: "Stress Test"} ]},
  {Key :"DERMLGY", Value: "Dermatology", SubCategories: [ {Key :"SKNTST", Value: "Skin Test"} ]}
];

let ResultRange: LookupData[] = [
  {Key: "A", Value: "Average"},
  {Key: "AA", Value: "Above Average"},
  {Key: "P", Value: "Poor"},
]

export { AnswersLookup,AnswersLookupNew, CategoryList, ResultRange }

export class PatientBasicInfo
{
  Name: string;
  EmailAddress: string;
  PatientGuid: string;
}

export class AssessmentInfo{
  AssessmentGuid: string;
  CategoryCd: string;
  SubCategoryCd: string;
  Title: string;
}

export class HP
{
  ProviderID: number;
  Name: string;
  ProviderGuid: string;
}

export class Appointments {
    AppointmentID: number = 0;;
    PatientID: number = 0;
    ProviderID: number = 0;
    Date: Date = new Date();
    StartTime: any;
    EndTime: Date;
    Status: boolean = false;
    IsUrgent: boolean = false;
    TimeBlock: string = "";
    Notes: string = "";
    Name: string = "";
}



export class PendingAppointment
{
    ProviderID:number = 0;
    Date:Date =  new Date();
}

export class PatientAssessmentInfo{
  ProviderGuid:string;
  ProviderName:string;
  AssessmentGuid:string;
  CategoryCd:string;
  SubCategoryCd:string;
  Title:string;
  ProviderImage:string;
  IsProviderImage:boolean;
}
export class PatientAssessmentResult{
  AssessmentGuid:string;
  AssessmentIntId:number;
  AssessmentTitle:string;
  Category:string;
  SubCategory:string;
  ProviderGuid:string;
  ProviderName:string;
  ProviderEmail:string;
  CompletedDate:Date;
  Result:string;
  Score:number;
  ViewResultDetail:boolean=true;
}
export class LoginResponse{
  expires:string;
  issued:string;
  AgreementFlg:boolean;
  access_token:string;
  expires_in:number;
  token_type:string;
  userName:string;
}


export class ChatContact {
  UserGuid: string;
  ImageBase64: string;
  ContactName: number;
  LastMessage: string;
  MessageDate: Date;
}

export class ChatModel {
  FromUser: boolean;
  UserName: string;
  Message: string;
  MessageDate: Date;
}

export class ChatRequest {
  userGuid: string;
  PageIndex: number = 1;
  PageSize: number = 40;
}


export class AddChatModal {
  UserGuid: string;
  Message: string;
}
export class RegisterModal {
    Email: string;
    Name: string;
    UserGuid : string;
    Password: string;
    ConfirmPassword: string;
    PhoneNumber: string;
    TermsAcceptedDate: Date;
    ServiceAcceptedDate: Date;
    ConsentAcceptedDate: Date;
    UserArea: number;
    UserGender: number;
    Birthdate: Date;
    Address: string;
    InsuranceCompany: string;
    StateLicenseNumber: string;
    UserProfession: number;
    UserSpecialty: number[];
    IsTempPassword: boolean = false;                                                                                                                                                                                                                         UserCode: string;
    CreatedDateTime : Date;
    LastUpdatedDateTime : Date;
}
export class QuestionAnswerQuiz
{
  constructor()
  {
    this.Answers = [new Answer()];
  }
			QuestionGuid: string;
			Question: string;
			OperationType: number;
      Answers: Answer[];
      AnswerString: string;
}
export class AssessmentQuestions
{
  constructor(){ }
  OperationType: number;
  Details: string;
  Quiz: QuestionAnswerQuiz[];
  ResultRange: ScoreRange[];
  Score:number;
  CreatedDate:DateTime;
  LastUpdatedDate:DateTime;
  ProviderName:string;
  Email:string;
  UserGuid: string;
  UserIntId: number;
  AssessmentIntId:number;
  PatientGuids: string[];
  AssessmentGuid: string;
	Title: string;
	CategoryCd: string;
	SubCategoryCd: string;
}
export class QuestionAnswerDetails{
  key:string;
  value:string;
}
export class SavePatient{
  AssessmentGuid: string;
  UserGuid: string;
  QuestionAnswer:QuestionAnswerDetails[];
  UserIntID:number;
}
export class ChangePasswordModel
{
  OldPassword:string;
  NewPassword:string;
  ConfirmPassword:string;
}

export class ForgotPasswordModel{
  Email:string;
}

export class RegisterResponse
{
  UserGuid:string;
  errorMessage:string;
}

export class FcmModal
{
  FcmToken:string;
}

export class PatientNotes
{
  Details: string;
  PatientGuid: string;
  IsNote: boolean
}


export class AssignAssessment
{
  PatientGuid: string;
  AssessmentGuid: string;
}

export class BusyDates
{
  BusyDate:Date[] = [];
}

export class dayConfig
{
    date: Date;
    cssClass:string;

}

export class UserBasicInfo
{
    Name: string;
    PatientGuid: string;
    EmailAddress: string;
    Details: string;
    Photo: string;
}
export class Provider
{
  BusyDate:Date[] = [];
}
