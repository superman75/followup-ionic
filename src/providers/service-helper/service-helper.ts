import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { LoadingController } from 'ionic-angular';
import { Assessment, PatientResult } from '../modals/modals'

/*
  Generated class for the ServiceHelperProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceHelperProvider {

    token: string;
    showingLoader: boolean = false;
    selectedAssessment: Assessment = null;
    selectedPatientResults: PatientResult[] = [];

    getSelectedAssessment(): Assessment {
        return this.selectedAssessment;
    }

    setSelectedAssessment(assessment: Assessment): void {
        this.selectedAssessment = assessment;
    }

    getSelectedPatientResults(): PatientResult[] {
        return this.selectedPatientResults;
    }

    setSelectedPatientResults(results: PatientResult[]) {
        this.selectedPatientResults = results;
    }
    // apiBaseUrl: string = 'http://relevant.southeastasia.cloudapp.azure.com/followup/';
    apiBaseUrl: string = 'http://followupservices.us-east-2.elasticbeanstalk.com/';

    //apiBaseUrl:string = 'http://localhost/followupservices/';
    // apiBaseUrl :string = 'http://ssingh-lt/followupservices/';
    // apiBaseUrl :string = 'http://follow.southeastasia.cloudapp.azure.com/followup/';
    loading: any = this.loadingCtrl.create({
        content: "Please wait..."
    });


    createLoader(message: string = "Please wait...") { // Optional Parameter
        this.loading = this.loadingCtrl.create({
            content: "Please wait..."
        });
    }

    constructor(public http: HttpClient, public loadingCtrl: LoadingController) {
        console.log('Hello ServiceHelperProvider Provider');
    }

    get(url) {
        this.createLoader();
        const headers = new HttpHeaders()
            .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
        this.loading.present();
        return this.http.get(this.apiBaseUrl + url, { headers })
            .map((resp: Response) => resp.json())
            .finally(() => {
                this.loading.dismiss();
            });
    }

    post(url, body) {
        this.createLoader();
        const headers = new HttpHeaders()
            .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
        this.loading.present();
        return this.http.post(this.apiBaseUrl + url, body, { headers })
            .map((resp: Response) => resp.json())
            .finally(() => {
                this.loading.dismiss();
            });
    }

    ////sample code for get
    getRequest(url) {
        //console.log( this.loading);
        if (!this.showingLoader) {
            this.showingLoader = true;
            this.createLoader();
            this.loading.present();
        }
        const headers = new HttpHeaders()
            .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
        // this.loading.present();
        return this.http.get(this.apiBaseUrl + url, { headers })
            .map((data: Response) => {
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                return data;
            }).catch((error: any) => {
                //this.loading.dismiss();
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }

            })
    }



    PostRequest(url, data) {
        if (!this.showingLoader) {
            this.showingLoader = true;
            this.createLoader();
            this.loading.present();
        }

        const headers = new HttpHeaders()
            .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));

        return this.http.post(this.apiBaseUrl + url, data, { headers })
            .map((res: Response) => {
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                return res;
            }).catch((error: any) => {
                // this.loading.dismiss();
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }

            })

    }
    PostRequestWithoutHeader(url, data) {
        if (!this.showingLoader) {
            this.showingLoader = true;
            this.createLoader();
            this.loading.present();
        }



        return this.http.post(this.apiBaseUrl + url, data)
            .map((res: Response) => {
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                return res;
            }).catch((error: any) => {
                // this.loading.dismiss();
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }

            })

    }




    PostRequestWOLoader(url, data) {

        const headers = new HttpHeaders()
            .set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
        return this.http.post(this.apiBaseUrl + url, data, { headers })
            .map(res => {

                return res;
            }, (err) => {

                console.log(err);
            });

    }

    //file upload event
    uploadFile(file: File, userGuid: string, isMedLicense: string): number {
        let result = null;
        //   let fileList: FileList = event.target.files;
        if (file != null) {
            //   let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            formData.append('IsMedLicense', isMedLicense);
            formData.append('UserGuid', userGuid);
            let headers = new HttpHeaders();
            // var options = new RequestOptions({ headers: headers });
            let apiUrl1 = this.apiBaseUrl + "api/Account/UploadFile";
            this.http.post(apiUrl1, formData, { headers })
                .map(res => { })
                .catch(error => Observable.throw(error))
                .subscribe(
                data => result = 1,
                error => result = 0)
        }

        return result;
    }


    //file upload event
    uploadProfileImage(file: File, userGuid: string): number {
        let result = null;
        //   let fileList: FileList = event.target.files;
        if (file != null) {
            //   let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append('uploadFile', file, file.name);
            formData.append('UserGuid', userGuid);
            let headers = new HttpHeaders();
            // var options = new RequestOptions({ headers: headers });
            let apiUrl1 = this.apiBaseUrl + "api/Message/UploadProfileImage";
            this.http.post(apiUrl1, formData, { headers })
                .map(res => { })
                .catch(error => Observable.throw(error))
                .subscribe(
                data => result = 1,
                error => result = 0)
        }

        return result;
    }


}
