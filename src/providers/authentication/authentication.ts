import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { LoadedproviderProvider } from '../loadedprovider/loadedprovider';
import { ServiceHelperProvider } from '../service-helper/service-helper';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { User } from '../../providers/modals/modals'
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the AuthenticationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationProvider {
    //apiBaseUrl: string = 'http://ssingh-lt/followupservices/';
    // apiBaseUrl: string  = 'http://follow.southeastasia.cloudapp.azure.com/followup/';
  //  apiBaseUrl: string = 'http://relevant.southeastasia.cloudapp.azure.com/followup/';
    apiBaseUrl: string = 'http://followupservices.us-east-2.elasticbeanstalk.com/';


    user: User = null;
    showingLoader: boolean = false;
    loading: any = this.loadingCtrl.create({
        content: "Please wait..."
    })

    createLoader(message: string = "Please wait...") { // Optional Parameter
        this.loading = this.loadingCtrl.create({
            content: "Please wait..."
        });
    }


    constructor(private http: HttpClient, private loader: LoadedproviderProvider, private serviceProvider: ServiceHelperProvider, public loadingCtrl: LoadingController) {

    }

    login(username: string, password: string, isPatient: boolean) {


        if (!this.showingLoader) {
            this.showingLoader = true;
            this.createLoader();
            this.loading.present();
        }


        let body = 'username=' + username + '&password=' + password + "{{isPatient=" + isPatient + "}}&grant_type=password";
        // body=encodeURIComponent(body);
        const headers = new HttpHeaders()
            .set("Content-Type", "application/x-www-form-urlencoded");


        return this.http.post(this.apiBaseUrl + 'token', body, { headers })
            .map((response: User) => {
                console.log(response);
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                // login successful if there's a jwt token in the response
                this.user = response;
                if (this.user && this.user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('ust', JSON.stringify(this.user.access_token));
                    this.serviceProvider.token = this.user.access_token;
                    return this.user;
                }

            }).catch((error: any) => {
                //this.loading.dismiss();
                if (this.showingLoader) {
                    this.showingLoader = false;
                    this.loading.dismiss();
                }
                if (error.status < 400 || error.status === 500) {
                    return Observable.throw(error);
                }

            })
    }



}
