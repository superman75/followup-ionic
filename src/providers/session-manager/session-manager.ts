import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User, FcmModal, PatientAssessmentInfo } from '../../providers/modals/modals';
/*
  Generated class for the SessionManagerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionManagerProvider {

  isPatient: boolean;
  userName: string;
  isTempPassword: string;
  tempPAssword: string;
  uName: string;
  userImage: string;

  toUserImage: string;
  showAssessmentReminder = false;
  patientAssessmentInfo: PatientAssessmentInfo;
  constructor(public http: HttpClient) {
    console.log('Hello SessionManagerProvider Provider');
  }
  setShowAssessmentReminder(value) {
    this.showAssessmentReminder = value;
  }
  getShowAssessmentReminder() {
    return this.showAssessmentReminder;
  }
  clearSessionVariables() {
    localStorage.removeItem('ust');
    localStorage.removeItem('userImage');
    localStorage.removeItem('isPatient');
    localStorage.removeItem('userGuid');

    this.isPatient = undefined;
    this.userName = undefined;
    this.isTempPassword = undefined;
    this.uName = undefined;
    this.showAssessmentReminder = false;
  }
  setRemiderAssessmentDetail(value) {
    this.patientAssessmentInfo = value;
  }
  getRemiderAssessmentDetail() {
    return this.patientAssessmentInfo;
  }
  //added comment for check in
  setPatient(isPatientvalue: boolean) {
    this.isPatient = isPatientvalue;
  }
  getPatient() {
    return this.isPatient;
  }

  setUserName(username: string) {
    this.userName = username;
  }
  getUserName() {
    return this.userName;
  }

  setisTempPassword(istempPAssword: string) {
    this.isTempPassword = istempPAssword;
  }
  getisTempPassword() {
    return this.isTempPassword;
  }

  setTempPassword(tempPAssword: string) {
    this.tempPAssword = tempPAssword;
  }
  getTempPassword() {
    return this.tempPAssword;
  }

  getName() {
    return this.uName;
  }

  setName(name: string) {
    this.uName = name;
  }


  getUserImage() {
    return this.userImage;
  }

  setUserImage(name: string) {
    this.userImage = name;
  }


  getToUserImage() {
    return this.toUserImage;
  }

  setToUserImage(name: string) {
    this.toUserImage = name;
  }


}
