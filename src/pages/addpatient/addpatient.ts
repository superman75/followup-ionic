import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';
import { Renderer } from '@angular/core';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper'
import {PatientBasicInfo} from '../../providers/modals/modals'


/**
 * Generated class for the AddpatientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'addpatient'
}
)
@Component({
  selector: 'page-addpatient',
  templateUrl: 'addpatient.html',
})
export class AddpatientPage {

  patientEmail: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer: Renderer,
    public viewCtrl: ViewController, public serviceProvider: ServiceHelperProvider) {
    
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
  }
  //https://www.techiediaries.com/ionic-modals/
  //https://stackoverflow.com/questions/42695169/ionic-2-login-popup-using-modal-and-styling

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpatientPage');
  }
  closeModal(){
    console.log('close');
    this.viewCtrl.dismiss();
  }

  searchPatients()
  {
    this.serviceProvider.getRequest("api/Assessment/SearchUsers?searchString="+ encodeURIComponent(this.patientEmail) + "&patients=true")
        .subscribe(
        (data: PatientBasicInfo[]) => {
          console.log(data);
          if(data != null && data.length == 1)
            {
                this.serviceProvider.PostRequest("api/Account/AddPatient?emailAddress="+ encodeURIComponent(data[0].EmailAddress), null)
                .subscribe(
                data => {
                  console.log(data);
                  this.closeModal();
                },
                error => {
                  console.log(error);
                  console.log("In Error");
                });
            }
        },
        error => {
          console.log(error);
          console.log("In Error");
        });
  }

}
