import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PatientResult,Gender,PatientAssessmentInfo } from '../../providers/modals/modals';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';
/**
 * Generated class for the AssessmentReminderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assessment-reminder',
  templateUrl: 'assessment-reminder.html',
})
export class AssessmentReminderPage {
  assessmentInfo: PatientAssessmentInfo;
  constructor(public navCtrl: NavController, public navParams: NavParams,public sessionManagerProvider:SessionManagerProvider) {
    this.assessmentInfo=this.navParams.get('selectedAssgnmnt');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssessmentReminderPage');
  }
  takeTest(){
    console.log('takeTest');
    this.sessionManagerProvider.setShowAssessmentReminder(false);
    this.sessionManagerProvider.setRemiderAssessmentDetail(undefined);
    this.navCtrl.push('AnwserQuestionPage',{selectedAssgnmnt:this.assessmentInfo});
  }
  skipTest(){
    this.sessionManagerProvider.setShowAssessmentReminder(false);
    this.sessionManagerProvider.setRemiderAssessmentDetail(undefined);
    this.navCtrl.push('PatientDashboardPage');
  }
}
