import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssessmentReminderPage } from './assessment-reminder';

@NgModule({
  declarations: [
    AssessmentReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(AssessmentReminderPage),
  ],
})
export class AssessmentReminderPageModule {}
