import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterModal, RegisterResponse } from '../../providers/modals/modals';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import { ToastProvider } from '../../providers/toast/toast';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  registerForm: FormGroup;
  speciality = "";
  specialityList = ['Dermatologist', 'Cardiologist'];
  professionList = ['Dermatologist', 'Cardiologist'];
  IsServiceAgreementChecked: boolean = false;
  IsConsentFormChecked: boolean = false;
  IsTermsAcceptedChecked: boolean = false;
  regUser: RegisterModal = new RegisterModal();
  regResponse: RegisterResponse = new RegisterResponse();
  profileImage: string = "";
  medLicenseFile: File = null;
  govIssuedIDFile: File = null;
  userImageFile: File = null;
  GuidForImage: string = "";
  GuidForDocument: string = "";
  constructor(public toastProvider: ToastProvider, public navCtrl: NavController, public navParams: NavParams, public serviceHelper: ServiceHelperProvider,
              private fb: FormBuilder) {
    this.regUser.UserArea = 2;
    this.registerForm = this.buildRegisterForm();
  }


  patient = true;

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  signIn() {
    console.log('Click of sign in!');
  }
  selectPatientSignIn() {
    console.log('Click of sign in!');
    this.patient = true;
    this.clearInputField();
    this.regUser.UserArea = 2;
  }
  selectDoctorSignIn() {
    console.log('Click of sign in!');
    this.patient = false;
    this.clearInputField();
    this.regUser.UserArea = 1;
  }

  backToLoginPage() {
    // this.navCtrl.push("LoginPage");
    this.navCtrl.popToRoot()
  }

  consentFormChecked() {
    this.IsConsentFormChecked = !this.IsConsentFormChecked;
    if (this.IsConsentFormChecked) {
      this.regUser.ConsentAcceptedDate = new Date();
    }
    else {
      this.regUser.ConsentAcceptedDate = null;
    }
  }

  // serviceAgreementChecked(){
  //   this.IsServiceAgreementChecked = !this.IsServiceAgreementChecked;
  //   if(this.IsServiceAgreementChecked)
  //   {
  //     this.regUser.ServiceAcceptedDate = new Date();
  //   }
  //   else
  //   {
  //     this.regUser.ServiceAcceptedDate = null;
  //   }
  // }

  termsAcceptedChecked() {
    this.IsTermsAcceptedChecked = !this.IsTermsAcceptedChecked;
    if (this.IsTermsAcceptedChecked) {
      this.regUser.TermsAcceptedDate = new Date();
    }
    else {
      this.regUser.TermsAcceptedDate = null;
    }
  }

  selectMedLicense(event) {
    let fileList: FileList = event.target.files;
    if (fileList != null && fileList.length > 0) {
      this.medLicenseFile = fileList[0];
    }
  }

  selectGovIssuedID(event) {
    let fileList: FileList = event.target.files;
    if (fileList != null && fileList.length > 0) {
      this.govIssuedIDFile = fileList[0];
    }
  }

  selectProfileImage(event) {
    let fileList: FileList = event.target.files;
    if (fileList != null && fileList.length > 0) {
      this.userImageFile = fileList[0];
    }


    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();

      reader.onload = (event: any) => {
        this.profileImage = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }

  }

  signUpRegister() {
    if (this.IsTermsAcceptedChecked) {
      this.regUser.Email = this.registerForm.get('email').value;
      this.regUser.Password = this.registerForm.get('password').value;
      if(this.registerForm.invalid){
        this.toastProvider.presentToast("Please insert valid Email and password should have capital letter and special character");
        return;
      }
      this.regUser.ConfirmPassword = this.regUser.Password;
      this.regUser.ServiceAcceptedDate = new Date();
      this.regUser.CreatedDateTime = new Date();
      this.regUser.LastUpdatedDateTime = new Date();
      this.regUser.UserGuid = this.newGuid();
      console.log(this.regUser);
      this.serviceHelper.PostRequestWithoutHeader("api/Account/Register", this.regUser)
        .subscribe(
        data => {
          console.log(JSON.stringify(data));
          this.regResponse = data;
          if(!data.UserGuid){
            this.toastProvider.presentToast(data.errorMessage);
            return;
          }
          this.GuidForImage = this.regResponse.UserGuid.split("{{}}")[1];
          this.GuidForDocument = this.regResponse.UserGuid.split("{{}}")[0];

          this.toastProvider.presentToast("User registered successfully.");
          if (this.regResponse.errorMessage == "" && this.IsConsentFormChecked && this.userImageFile != null) {
            //this.serviceHelper.uploadProfileImage(this.userImageFile, this.regResponse.UserGuid);
            this.serviceHelper.uploadProfileImage(this.userImageFile, this.GuidForImage);
            if (this.patient == true) {
              setTimeout(() => {    //<<<---    using ()=> syntax
                this.navCtrl.popToRoot();
              }, 6000);
            }
          }
          if (this.regResponse.errorMessage == "" && this.patient == false) {
            this.serviceHelper.uploadFile(this.medLicenseFile, this.GuidForDocument, "true");
            this.serviceHelper.uploadFile(this.govIssuedIDFile, this.GuidForDocument, "false");
            this.toastProvider.presentToast("User registered successfully.");
            setTimeout(() => {    //<<<---    using ()=> syntax
              this.navCtrl.popToRoot();
            }, 6000);
          }
          else {
            this.toastProvider.presentToast(this.regResponse.errorMessage);
          }
        },
        error => {
          //this.toastProvider.presentToast(error.error.ExceptionMessage);
          this.toastProvider.presentToast("There was some error registering user, please ensure you have used unique email Id or please try again later.");
        });
    }

    else {
      this.toastProvider.presentToast("Please Accept terms of service and privacy policy.");
    }
  }

  signUp() {

    if (this.userImageFile) {
      if (!(this.IsConsentFormChecked)) {
        this.toastProvider.presentToast("Please Accept Consent Form in order to upload image.");
        return;
      }
    }

    if (this.IsConsentFormChecked) {
      if (this.userImageFile == null) {
        this.toastProvider.presentToast("Please upload a profile image or uncheck consent form.");
        return;
      }
    }

    if (this.patient == false) {
      if (this.medLicenseFile == null) {
        this.toastProvider.presentToast("Please upload Medical License.");
        return;
      }
      else if (this.govIssuedIDFile == null) {
        this.toastProvider.presentToast("Please upload Government ISsued ID.");
        return;
      }
    }

    this.signUpRegister();
    //console.log(this.speciality);
    /* if(this.IsServiceAgreementChecked)
     {
       this.regUser.ConfirmPassword = this.regUser.Password;
       this.regUser.ServiceAcceptedDate = new Date();
       this.regUser.TermsAcceptedDate = new Date();
       this.serviceHelper.PostRequest("api/Account/Register", this.regUser)
           .subscribe(
           data => {
             this.regResponse = <RegisterResponse> data;
             if(this.regResponse.errorMessage == "" && this.patient == false)
             {
               this.serviceHelper.uploadFile(this.medLicenseFile, this.regResponse.UserGuid, "true")
                 this.serviceHelper.uploadFile(this.govIssuedIDFile, this.regResponse.UserGuid, "false")
               this.toastProvider.presentToast("User registered successfully.");
               setTimeout(()=>{    //<<<---    using ()=> syntax
                 this.navCtrl.popToRoot();
               },6000);
             }
             else
             {
                 this.toastProvider.presentToast(this.regResponse.errorMessage);
             }
           },
           error => {
               //this.toastProvider.presentToast(error.error.ExceptionMessage);
               this.toastProvider.presentToast("There was some error registering user, please ensure you have used unique email Id or please try again later.");
           });
   }

     else
     {
       this.toastProvider.presentToast("Please Accept terms of service and privacy policy.");
     }*/
  }
  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  }

  buildRegisterForm() {
    return this.fb.group({
      // 'email': ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      // 'password': ['', Validators.required]
      email: ['', [Validators.required, Validators.pattern(/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/)]],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.maxLength(100),Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!-/])[a-zA-Z0-9!-/]+$')])]  ,
      PhoneNumber: [''],
      name : [''],
      gender : [''],
      Birthdate : [''],
      Address : [''],
      InsuranceCompany : [''],
      StateLicenseNumber : [''],
      UserProfession : [''],
      speciality : ['']
    });
  }
  clearInputField() {
    this.regUser.Name = "";
    this.regUser.Password = "";
    this.regUser.Email = "";
    this.regUser.Address = "";
    this.regUser.Birthdate = "";
    this.regUser.InsuranceCompany = "";
    this.regUser.PhoneNumber = null;
    this.regUser.UserGender = null;
    this.regUser.UserProfession = null;
    this.regUser.UserSpecialty = null;
    this.medLicenseFile = null;
    this.govIssuedIDFile = null;
    this.userImageFile = null;
    this.profileImage = null;
  }
}

