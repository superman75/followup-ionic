import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartQuizPage } from './start-quiz';
import { NavBarLightComponent } from '../../components/nav-bar-light/nav-bar-light';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    StartQuizPage,
    
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(StartQuizPage),
  ],
})
export class StartQuizPageModule {}
