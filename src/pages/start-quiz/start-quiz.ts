import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NavBarLightComponent } from '../../components/nav-bar-light/nav-bar-light';
import { PatientResult,Gender,PatientAssessmentInfo } from '../../providers/modals/modals';

/**
 * Generated class for the StartQuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start-quiz',
  templateUrl: 'start-quiz.html',
})
export class StartQuizPage {
  assessmentGuid:string;
  assessmentInfo: PatientAssessmentInfo;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
    //this.assessmentGuid = this.navParams.get('selectedAssgnmnt');
    
    this.assessmentInfo=this.navParams.get('selectedAssgnmnt');
    console.log(this.assessmentGuid);
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StartQuizPage');
  }
  takeTest(){
    console.log('takeTest');
    this.navCtrl.push('AnwserQuestionPage',{selectedAssgnmnt:this.assessmentInfo});
  }
}
