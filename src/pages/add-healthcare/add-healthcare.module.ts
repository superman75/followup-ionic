import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddHealthcarePage } from './add-healthcare';

@NgModule({
  declarations: [
    AddHealthcarePage,
  ],
  imports: [
    IonicPageModule.forChild(AddHealthcarePage),
  ],
})
export class AddHealthcarePageModule {}
