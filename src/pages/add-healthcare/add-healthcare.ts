import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {ToastProvider} from '../../providers/toast/toast'
import {PatientBasicInfo} from '../../providers/modals/modals';
import { Http, RequestOptions, Headers, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx';
import { HttpModule } from '@angular/http';
import { HttpClient,HttpHeaders } from '@angular/common/http';


/**
 * Generated class for the AddHealthcarePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-healthcare',
  templateUrl: 'add-healthcare.html',
})
export class AddHealthcarePage {

  providerCode: string = "";
  constructor(public http: HttpClient,public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
    public renderer: Renderer, public serviceProvider: ServiceHelperProvider) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup1', true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddHealthcarePage');
  }
  closeModal(){
    console.log('close');
    this.viewCtrl.dismiss();
  }

  addHealthProvider()
  {
    
    this.serviceProvider.getRequest("api/Assessment/SearchUsers?searchString="+ encodeURIComponent(this.providerCode) + "&patients=false")
        .subscribe(
        (data: PatientBasicInfo[]) => {
          console.log(data);
          if(data != null && data.length == 1)
            {
                this.serviceProvider.PostRequest("api/Assessment/AddExistingProvider?providerGuid="+ encodeURIComponent(data[0].PatientGuid), null)
                .subscribe(
                data => {
                  console.log(data);
                  this.closeModal();
                },
                error => {
                  console.log(error);
                  console.log("In Error");
                });
            }
        },
        error => {
          console.log(error);
          console.log("In Error");
        });
  }
  
    //file upload event  
fileChange(event) {
  debugger;
  let fileList: FileList = event.target.files;  
  if (fileList.length > 0)
    {
      let file: File = fileList[0];  
      let formData: FormData = new FormData();  
      formData.append('uploadFile', file, file.name);
      formData.append('IsMedLicense', 'true');
      formData.append('UserGuid', '1198091E-F257-42F4-BC93-F3E8B9FF6427');
      let headers = new HttpHeaders();
      headers.set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
     // var options = new RequestOptions({ headers: headers });  
      let apiUrl1 = "http://localhost/followupservices/api/Assessment/UploadFile";  
      this.http.post(apiUrl1, formData, {headers})  
      .map(res =>{})  
      .catch(error => Observable.throw(error))  
      .subscribe(  
      data => console.log('success'),  
      error => console.log(error))  
    }
    window.location.reload();  
  }  
}
