import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//import { HptabsComponent } from 'HptabsComponent';


import { HptabsComponent } from '../../components/hptabs/hptabs';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  openChat(){
    this.navCtrl.push('MessagePage');
    }

}
