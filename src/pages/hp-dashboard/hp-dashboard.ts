import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HptabsComponent } from '../../components/hptabs/hptabs';
import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { AddpatientPage } from '../../pages/addpatient/addpatient';
import { CreateAssignmentPage } from '../../pages/create-assignment/create-assignment';

import { ModalController } from 'ionic-angular';

/**
 * Generated class for the HpDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hp-dashboard',
  templateUrl: 'hp-dashboard.html',
})
export class HpDashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl : ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HpDashboardPage');
  }

  public addPatient(){
    console.log('AddPatient');
    var modalPage = this.modalCtrl.create('addpatient'); 
    modalPage.present(); 
  }
  public createAssignment1(){
    console.log('AddPatient');
    var modalPage = this.modalCtrl.create('CreateAssignmentPage'); 
    modalPage.present(); 
  }
  public createAssignment(){
    
    var modalPage = this.modalCtrl.create('CreateAssessmentPage'); 
    modalPage.present(); 
  }
  public viewClients(){
    this.navCtrl.push('ExistngPatientPage',{selectedTab:"patient"});
  }
  public seeAssessments(){
    this.navCtrl.push('AssessmentsPage');
  }
  

}
