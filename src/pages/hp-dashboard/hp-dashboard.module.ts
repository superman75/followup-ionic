import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HpDashboardPage } from './hp-dashboard';
// import { HptabsComponent } from '../../components/hptabs/hptabs';
// import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    HpDashboardPage,
    // HptabsComponent,
    // TopNavbarComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(HpDashboardPage),
  ],
})
export class HpDashboardPageModule {}


