import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {AddpatientPage} from '../addpatient/addpatient';
import {AddHealthcarePage} from '../add-healthcare/add-healthcare';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import { concat } from 'rxjs/operators/concat';
import {ToastProvider } from '../../providers/toast/toast';
import { PatientResult,Gender,PatientAssessmentInfo } from '../../providers/modals/modals';

/**
 * Generated class for the PatientDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patient-dashboard',
  templateUrl: 'patient-dashboard.html',
})
export class PatientDashboardPage {
  
  patientAssessmentInfo:PatientAssessmentInfo[]=[];
  HealthProvider:PatientAssessmentInfo[];
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl : ModalController,public serviceHelperProvider:ServiceHelperProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientDashboardPage');
    this.loadPatientAssessment();
  }
  loadPatientAssessment() {
    
        this.serviceHelperProvider.getRequest("api/Assessment/GetPatientAssessmentList")
        .subscribe(
            data => {
              
              console.log(data);
              this.patientAssessmentInfo= <PatientAssessmentInfo[]>data;
              
              
             
              this.HealthProvider = <PatientAssessmentInfo[]>data;
              
              this.HealthProvider=   this.HealthProvider.filter((e, i) => this.HealthProvider.findIndex(a => a["ProviderGuid"] === e["ProviderGuid"]) === i);
              
              
                this.getProviderImage();
             

             
              console.log(data);
            },
            error => {
             console.log(error);
            });
  }

  // addHealthProvider(){
  //   var modalPage = this.modalCtrl.create('AddHealthcarePage'); 
  //   modalPage.present(); 
  // }
  startQuiz(item:any){
    console.log(item)
 //go to start quiz page
 this.navCtrl.push('StartQuizPage',{selectedAssgnmnt:item});
  }
  getProviderImage(){
    this.HealthProvider.forEach(element => {
  

      this.serviceHelperProvider.PostRequest("api/Message/GetUserProfileImage?userGuid="+element.ProviderGuid,"")
      .subscribe(
      data => {
        // ProviderImage:string;
        // IsProviderImage:boolean; 
        if (data!="") {
         // element.IsProviderImage=true;
          element.ProviderImage="data:image/png;base64,"+data;
        } else {
         // element.IsProviderImage=false;
          element.ProviderImage="";
        }
       
       
      },
      error => {
          console.log("In Error");
      });

  });


  }
  
}
