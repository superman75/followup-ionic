import { Component, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthenticationProvider } from './../../providers/authentication/authentication';
import { LoadingController } from 'ionic-angular';
import {User,FcmModal,PatientAssessmentInfo} from '../../providers/modals/modals';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';
import { FCM } from '@ionic-native/fcm';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import { Platform } from 'ionic-angular';
import {ToastProvider } from '../../providers/toast/toast';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  patientAssessmentInfo:PatientAssessmentInfo[]=[];
 // userEmail:string = "kedar.mahagaonkar+14@gmail.com";
  //password:string = "Aa123456#";
  userEmail:string ="";
 /* loginGroup:FormGroup = new FormGroup({
    userValidator: new FormControl('', Validators.compose([
      Validators.required,
      Validators.email
    ]))
 });*/

  password:string = "Aa123456#";
  //test@test.comm / Techm#005

  //HP
  // userEmail:string = "satish0902@gmail.com";
  // password:string = "Techm#005";

  //userEmail:string = "testani@test.com";
  //password:string = "Techm#005";
  user:User = new User();
  fcmDevice:FcmModal = new FcmModal();


  constructor(public toastProvider: ToastProvider,public plt: Platform,private serviceHelperProvider:ServiceHelperProvider,private fcm: FCM,public navCtrl: NavController, public navParams: NavParams,public authenticationProvider:AuthenticationProvider,public loadingController:LoadingController,public sessionManagerProvider:SessionManagerProvider) {
  }
  patient=true
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  signIn() {
    this.sessionManagerProvider.setPatient(this.patient);
    console.log('Click of sign in test!');
    this.userEmail = this.userEmail;
    console.log(this.userEmail);
    this.authenticationProvider.login(this.userEmail, this.password, this.patient)
    .subscribe(
        data => {
          console.log(data);
         // this.registerDevice();
          this.user = data;
          this.sessionManagerProvider.setisTempPassword(this.user.IsTempPassword);
          this.sessionManagerProvider.setName(this.user.Name);
          
          localStorage.setItem('isPatient', JSON.stringify(this.patient));
          localStorage.setItem('userGuid', JSON.stringify(this.user.UserGuid));
          
          this.getAssessment();
           
         
        },
        error => {
          this.toastProvider.presentToast("Please check your username or password or please select correct user type.");
        // console.log("In Error");
        });
    
  }

getProfileImage()
{
  this.serviceHelperProvider.getRequest("api/Message/GetUserImage")
  .subscribe(
  data => {
    this.sessionManagerProvider.setUserImage(data);
    localStorage.setItem('userImage', JSON.stringify(data));
      //console.log(this.msgList);
  },
  error => {
      console.log("In Error");
  });
}

getAssessment()
{
  this.serviceHelperProvider.getRequest("api/Assessment/GetPatientAssessmentList")
  .subscribe(
  data => {
    console.log(data);
    this.patientAssessmentInfo= <PatientAssessmentInfo[]>data;
    //In case of pending assignment more than one redirect user to reminder page
    if (this.patientAssessmentInfo.length>0) {
      this.sessionManagerProvider.setShowAssessmentReminder(true);
      this.sessionManagerProvider.setRemiderAssessmentDetail(this.patientAssessmentInfo[0]);
    } else {
      this.sessionManagerProvider.setShowAssessmentReminder(false);
      this.sessionManagerProvider.setRemiderAssessmentDetail(undefined);
    }
    this.navigateToForwardPages();
   
  },
  error => {
      console.log("In Error");
  });
}
navigateToForwardPages()
{
  this.getProfileImage();
  if(this.user.AgreementFlg == "True")
  {
      this.navCtrl.push('SrvcAgrmntPage');
  }
  else if (this.user.IsTempPassword == "True")
  {
    this.sessionManagerProvider.setTempPassword(this.password);
    this.navCtrl.push('ChangePasswordPage');
  }
  else
  { 
    if (this.sessionManagerProvider.getShowAssessmentReminder()) {
      this.navCtrl.push('AssessmentReminderPage',{selectedAssgnmnt:this.patientAssessmentInfo[0]});
    } else {
      if(this.patient){
        this.navCtrl.push('PatientDashboardPage');
      }else{
        this.navCtrl.push('HpDashboardPage');
      }
    }
    
  }
}

  selectPatientSignIn() {
    console.log('Click of sign in!');
    this.patient=true;
  }
  selectDoctorSignIn() {
    console.log('Click of sign in!');
    this.patient=false;
  }

  goToSignUp(){
    console.log('Click goToSignUp!');
    this.navCtrl.push('SignUpPage');
  }

  goToForgotPasswordPage(){
    this.navCtrl.push('ForgotPasswordPage');
  }

  registerDevice()
  {
    this.plt.ready().then((readySource) => {
      this.fcm.getToken().then(token => {
        this.fcmDevice.FcmToken = token;
        this.serviceHelperProvider.PostRequest("api/fcm/RegisterDevice", this.fcmDevice).subscribe(
         data => {
          
         },
         error => {
         });
     });
    });
   
  }

}
