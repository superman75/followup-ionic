import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HptabsComponent } from '../../components/hptabs/hptabs';
import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar'
import { HP,Appointments,PendingAppointment,dayConfig,BusyDates } from '../../providers/modals/modals';
import {ToastProvider } from '../../providers/toast/toast';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';


/**
 * Generated class for the DctrSchdlDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dctr-schdl-detail',
  templateUrl: 'dctr-schdl-detail.html',
})
export class DctrSchdlDetailPage {
 // date: string;
  date: any;
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  _daysConfig: DayConfig[] = [];
  tempConfig:dayConfig= new dayConfig();
  busyDates:BusyDates = new BusyDates();

  calendarOption: CalendarComponentOptions = { pickMode: 'single', showToggleButtons: true,daysConfig:this._daysConfig};
 // calendarOption: CalendarComponentOptions = { pickMode: 'single', showToggleButtons: true};
  patientAppointments: Appointments[]=[];
  pendingAppointments: Appointments[]=[];

  pendingAppointmentInput:PendingAppointment = new PendingAppointment();
 

  myDate="";
  constructor(public toastProvider: ToastProvider,public navCtrl: NavController, public navParams: NavParams, public serviceHelper: ServiceHelperProvider) {
   // this.myDate='07:43';
    this.LoadWeekAppointments();
    this.LoadUnconfirmedAppointments(new Date());
    this. LoadBusyDates();
  }

  LoadBusyDates()
  {
    this.serviceHelper.getRequest("api/Appointment/GetBusyDates")
    .subscribe(
        data => {
         this.busyDates = <BusyDates> data;
         if(this.busyDates.BusyDate)
         {
         this.busyDates.BusyDate.forEach((item) => { // foreach statement  
          this.tempConfig= new dayConfig();
          this.tempConfig.date = item;
          this.tempConfig.cssClass='dateunavailable';

          this._daysConfig.push(this.tempConfig);
       }) 
      }
       this.calendarOption = { pickMode: 'single', showToggleButtons: true,daysConfig:this._daysConfig};
        },
        error => {
        // this.toastProvider.presentToast("Service Unavailable. Please try after some time.");
        });

  }


  onChange($event) {
   // console.log(this.date.utc());
    this.LoadUnconfirmedAppointments(new Date(this.date._d));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DctrSchdlDetailPage');
    
  }

  LoadWeekAppointments()
  {

    this.serviceHelper.getRequest("api/Appointment/GetHPAppointment")
    .subscribe(
        data => {
          this.patientAppointments = <Appointments[]> data;
          console.log(this.pendingAppointments);
        },
        error => {
         console.log("In Error");
        });
  }


  LoadUnconfirmedAppointments(date)
  {
    var temp;
    try{
        temp = date.toDateString();
    }
    catch(Exception)
    {
      temp = date;
    }
    this.pendingAppointments = [];
    this.pendingAppointmentInput.Date = new Date(temp);
    this.pendingAppointmentInput.Date.setDate( this.pendingAppointmentInput.Date.getDate() + 1 );
    this.serviceHelper.PostRequest("api/Appointment/GetUnConfirmedHPAppointment",this.pendingAppointmentInput)
    .subscribe(
        data => {
          var temp = <Appointments[]> data;
         try
         {
            temp.forEach((item) => { // foreach statement  
              item.StartTime = item.TimeBlock.substring(0, 2)+ ":00";
              this.pendingAppointments.push(item);
          }) 
        }
        catch(Exception)
        {
          
        } 
       
         
          console.log(this.pendingAppointments);
        },
        error => {
         console.log("In Error");
        });

  }

  DeleteAppointment(item)
  {
    this.serviceHelper.PostRequest("api/Appointment/DeleteAppointment",item)
    .subscribe(
        data => {
         console.log(data);
         this.LoadWeekAppointments();
         this.toastProvider.presentToast("Appointment Deleted successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while deleting appointment, Please try again later.");
        });
  }

  UpdateUrgetStatus(item)
  {

    this.serviceHelper.PostRequest("api/Appointment/ManageUrgentAppointment",item)
    .subscribe(
        data => {
         console.log(data);
         this.toastProvider.presentToast("Appointment updated successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while booking appointment, Please try again later.");
        });

  }

  BookAppointment(item)
  {
    item.TimeBlock = item.StartTime;
    this.serviceHelper.PostRequest("api/Appointment/ManageAppointment",item)
    .subscribe(
        data => {
         this.LoadWeekAppointments();
         this. LoadUnconfirmedAppointments(item.Date);
         this.toastProvider.presentToast("Appointment Confirmed successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while confirming appointment, Please try again later.");
        });
  } 



}
