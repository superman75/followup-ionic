import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DctrSchdlDetailPage } from './dctr-schdl-detail';
// import { HptabsComponent } from '../../components/hptabs/hptabs';
// import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { ComponentsModule } from '../../components/components.module';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    DctrSchdlDetailPage,
    // HptabsComponent,
    // TopNavbarComponent,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(DctrSchdlDetailPage),
    CalendarModule
    
  ],
})
export class DctrSchdlDetailPageModule {}
