import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientProfilePage } from './patient-profile';
import { ComponentsModule } from '../../components/components.module';
import { CalendarModule } from "ion2-calendar";

@NgModule({
  declarations: [
    PatientProfilePage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PatientProfilePage),
    CalendarModule
  ],
})
export class PatientProfilePageModule {}
