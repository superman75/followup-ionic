import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import {
  PatientResult, Gender, PatientNotes, PatientAssessmentInfo, AssessmentQuestions,
  QuestionAnswerQuiz, SavePatient, QuestionAnswerDetails
} from '../../providers/modals/modals';
import { ToastProvider } from '../../providers/toast/toast';
import { ModalController } from 'ionic-angular';

/**
 * Generated class for the PatientProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patient-profile',
  templateUrl: 'patient-profile.html',
})
export class PatientProfilePage {

  selectedPatientResults: PatientResult[] = [];
  patientName: string = "";
  patientAge: number = 0;
  patientGender: string = "";
  patientId: number = 0;
  patientGuid: string = "";
  patientNotes: string = "";
  patientDiagnosis: string = "";
  patientImage: string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public serviceProvider: ServiceHelperProvider, public toastProvider: ToastProvider, public modalCtrl: ModalController) {
    this.selectedPatientResults = this.serviceProvider.getSelectedPatientResults();

    if (this.selectedPatientResults != null && this.selectedPatientResults.length > 0) {
      this.patientName = this.selectedPatientResults[0].Name;
      this.patientAge = this.selectedPatientResults[0].Age;
      this.patientId = this.selectedPatientResults[0].PatientIntId;
      this.patientNotes = this.selectedPatientResults[0].Notes;
      this.patientDiagnosis = this.selectedPatientResults[0].Diagnosis;
      this.patientGuid = this.selectedPatientResults[0].PatientGuid;
      if (this.selectedPatientResults[0].UserGender == Gender.Male)
        this.patientGender = "Male";
      else if (this.selectedPatientResults[0].UserGender == Gender.Female)
        this.patientGender = "Female";
    }
    this.getPatientImage();

  }

  getPatientImage() {
    this.serviceProvider.PostRequest("api/Message/GetUserProfileImage?userGuid=" + this.patientGuid, "")
      .subscribe(
      data => {
        if (data == "") {
          this.patientImage = "";
        } else {

          this.patientImage = "data:image/png;base64," + data;
        }
      },
      error => {
        console.log("In Error");
      });
  }
  AddNotesAndDiagnosis(isNote: boolean) {
    let url: string = 'api/Assessment/AddNotesAndDiagnosis';
    let notes: PatientNotes = new PatientNotes();
    notes.IsNote = isNote;
    notes.PatientGuid = this.patientGuid;
    notes.Details = isNote ? this.patientNotes : this.patientDiagnosis;

    this.serviceProvider.PostRequest(url, notes)
      .subscribe(
      data => {
        this.toastProvider.presentToast("Patient " + isNote ? "notes" : "diagnosis" + " updated successfully.");
      },
      error => {
        console.log("In Error");
        this.toastProvider.presentToast("Error updating patient " + isNote ? "notes" : "diagnosis" + " , Please try again later.");
      });
  }

  goToAssessmentResult(item: PatientResult) {

    if (item.CompletedDate == null) {
      this.toastProvider.presentToast("Patient has not submitted this assessment. There are no details to deplay.");
      return;
    }

    let info: PatientAssessmentInfo = null;
    let assessmentDetail: AssessmentQuestions = null;
    let quiz: QuestionAnswerQuiz[] = null;
    let url: string = 'api/Assessment/GetAssessment?assessmentGuid=' + item.AssessmentGuid;
    this.serviceProvider.getRequest(url)
      .subscribe(
      (data: AssessmentQuestions) => {
        assessmentDetail = data;
        quiz = assessmentDetail.Quiz;
        info = new PatientAssessmentInfo();
        info.AssessmentGuid = data.AssessmentGuid;
        info.CategoryCd = data.CategoryCd;
        info.SubCategoryCd = data.SubCategoryCd;
        info.Title = data.Title;

        this.serviceProvider.getRequest("api/Assessment/GetPatientAssessedDetails?assessmentGuid=" +
          item.AssessmentGuid + "&patientGuid=" + item.PatientGuid)
          .subscribe(
          (data) => {
            console.log(data);
            quiz.forEach(q => {
              data.QuestionAnswer.forEach(a => {
                console.log(a);
                if (q.QuestionGuid == a.Key) {
                  q.AnswerString = a.Value;
                }
              });
            });

            this.navCtrl.push('SubmitQuizPage', { selectedAssgnmnt: info, quiz: quiz, assessmentDetail: assessmentDetail });

          },
          error => {
            console.log(error);
          });
      },
      error => {
        console.log("In Error");
        this.toastProvider.presentToast("Error while fetching assessment info, Please try again later.");
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientProfilePage');
  }

  selectAssessment() {
    // var modalPage = this.modalCtrl.create('SelectAssessmentPage', { PatientGuid: this.patientGuid}); 
    // modalPage.present(); 
    this.navCtrl.push('AssessmentsPage');
  }

}
