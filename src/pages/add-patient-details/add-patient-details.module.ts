import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPatientDetailsPage } from './add-patient-details';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddPatientDetailsPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AddPatientDetailsPage),
  ],
})
export class AddPatientDetailsPageModule {}
