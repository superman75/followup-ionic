import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatContact } from '../../providers/modals/modals';
import {ToastProvider } from '../../providers/toast/toast';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';

//import { ChatComponent } from '../../components/chat/chat';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

    chatContacts: ChatContact[] = [];
    isPAtient:boolean = false;

  constructor(public sessionManagerProvider:SessionManagerProvider,public toastProvider: ToastProvider, public navCtrl: NavController, public navParams: NavParams, public serviceHelper: ServiceHelperProvider) {
    this.isPAtient = this.sessionManagerProvider.getPatient(); 
    this.LoadMessageContacts();
  }

  
  openChat(item) {
        this.sessionManagerProvider.setToUserImage(item.ImageBase64);
        let jsonData = JSON.stringify(item);
        this.navCtrl.push('MessageListPage', { jsonData: jsonData });
    }

    LoadMessageContacts() {

        this.serviceHelper.getRequest("api/Message/GetChatContact")
            .subscribe(
            data => {
                this.chatContacts = <ChatContact[]>data;
                console.log(this.chatContacts);
            },
            error => {
                console.log("In Error");
            });
    }

}
