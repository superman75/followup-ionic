import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {ToastProvider} from '../../providers/toast/toast'
import {BusyDates,dayConfig} from '../../providers/modals/modals';
import { Http, RequestOptions, Headers, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx';
import { HttpModule } from '@angular/http';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  providerCode: string = "";
  dateMulti: Date[];
  selectedBusyDates:BusyDates = new BusyDates();
  busyDates:BusyDates = new BusyDates();
  currentUserImage:string ="";
  userGuid:string = "";
  profileImage:string="";
  type: 'js-date'; // 'string' | 'js-date' | 'moment' | 'time' | 'object'
  /*_daysConfig: DayConfig[] = [{
    date: new Date(2018, 3, 29),
    cssClass:'dateunavailable'
  }];*/
  _daysConfig: DayConfig[] = [];
  tempConfig:dayConfig= new dayConfig();
  calendarOption: CalendarComponentOptions = { pickMode: 'multi', showToggleButtons: true,daysConfig:this._daysConfig};
 //calendarOption: CalendarComponentOptions = { pickMode: 'multi', showToggleButtons: true};
 isPatient:boolean = false;
 IsConsentFormChecked:boolean=false;
 ispatientstr:string ="";
 showImage:boolean=false;
 userImageFile: File = null;

  constructor(public sessionManagerProvider:SessionManagerProvider, public toastProvider:ToastProvider ,public http: HttpClient,public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
    public renderer: Renderer, public serviceProvider: ServiceHelperProvider) {
   // this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup1', true);
   this.dateMulti = [];
   //this.dateMulti.push(new Date("05/05/2018"));
   this.ispatientstr =  localStorage.getItem('isPatient').replace(/"/g, "");
   this.userGuid =  localStorage.getItem('userGuid').replace(/"/g, ""); //userGuid
   if(this.ispatientstr.toLowerCase() == "true")
     this.isPatient = true;
   //isPatient
   console.log("Is PAtient " + this.isPatient)
   if(!this.isPatient)
      this.LoadBusyDates();

      var temp = localStorage.getItem('userImage').replace(/"/g, "");
      if(temp)
      {
        this.IsConsentFormChecked = true;
        this.currentUserImage = "data:image/png;base64," + temp;
        this.showImage = true;
        this.profileImage = "data:image/png;base64," + temp;
      }
      else
      {
        this.showImage = false;
        this.IsConsentFormChecked = false;
        this.currentUserImage = null;
      }
      console.log( this.currentUserImage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddHealthcarePage');
  }

  consentFormChecked(){
    this.IsConsentFormChecked = !this.IsConsentFormChecked;

  }

  getProfileImage()
{
  this.serviceProvider.getRequest("api/Message/GetUserImage")
  .subscribe(
  data => {
    this.sessionManagerProvider.setUserImage(data);
    localStorage.setItem('userImage', JSON.stringify(data));
      //console.log(this.msgList);
  },
  error => {
      console.log("In Error");
  });
}

  updateProfileImage()
  {
    if(this.IsConsentFormChecked)
    {
      this.serviceProvider.uploadProfileImage(this.userImageFile, this.userGuid);
       setTimeout(()=>{    //<<<---    using ()=> syntax
                this.getProfileImage();
              },4000);
      this.toastProvider.presentToast("Profile Image uploaded successfully.");
    }
    else
      this.toastProvider.presentToast("Please upload an image and check the consent form.");
  }

  selectProfileImage(event)
  {

    let fileList: FileList = event.target.files;
    if(fileList != null && fileList.length > 0)
      {
          this.userImageFile = fileList[0];
      }


      if(event.target.files && event.target.files[0]){
        let reader = new FileReader();
  
        reader.onload = (event:any) => {
          this.profileImage = event.target.result;
          this.showImage = true;
        }
        reader.readAsDataURL(event.target.files[0]);
      }
  }

  onChange($event) {
   // console.log(this.date.utc());
console.log(this.dateMulti);
  }

  LoadBusyDates()
  {
    this.serviceProvider.getRequest("api/Appointment/GetBusyDates")
    .subscribe(
        data => {
         this.busyDates = <BusyDates> data;
         this.dateMulti = [];
         try
         {
        if(this.busyDates.BusyDate.length > 0)
         {
         this.busyDates.BusyDate.forEach((item) => { // foreach statement  
          this.tempConfig= new dayConfig();
          this.tempConfig.date = item;
          this.tempConfig.cssClass='dateunavailable';

          this._daysConfig.push(this.tempConfig);
          this.dateMulti.push(item);
         
       }) 
        }
      }
      catch(Exception)
      {}
       this.calendarOption = { pickMode: 'multi', showToggleButtons: true,daysConfig:this._daysConfig};
       console.log(this.dateMulti);
         console.log(this._daysConfig);
        },
        error => {
        // this.toastProvider.presentToast("Service Unavailable. Please try after some time.");
        });

  }

  addBusyDays()
  {
    try
    {
      this.selectedBusyDates.BusyDate=[];
       this.dateMulti.forEach((item) => { // foreach statement  
        var temp = new Date(item);
        temp.setDate( temp.getDate() + 1)
        this.selectedBusyDates.BusyDate.push(temp);
     }) 

     this.serviceProvider.PostRequest("api/Appointment/UpdateBusyDates",this.selectedBusyDates)
     .subscribe(
         data => {
          this.toastProvider.presentToast("Dates updated successfully.");
         },
         error => {
          this.toastProvider.presentToast("Service Unavailable. Please try after some time.");
         });



   }
   catch(Exception)
   {
     
   } 
   
  }


 
  
    //file upload event  
fileChange(event) {
  debugger;
  let fileList: FileList = event.target.files;  
  if (fileList.length > 0)
    {
      let file: File = fileList[0];  
      let formData: FormData = new FormData();  
      formData.append('uploadFile', file, file.name);
      formData.append('IsMedLicense', 'true');
      formData.append('UserGuid', '1198091E-F257-42F4-BC93-F3E8B9FF6427');
      let headers = new HttpHeaders();
      headers.set("Authorization", "Bearer " + localStorage.getItem('ust').replace(/"/g, ""));
     // var options = new RequestOptions({ headers: headers });  
      let apiUrl1 = "http://localhost/followupservices/api/Assessment/UploadFile";  
      this.http.post(apiUrl1, formData, {headers})  
      .map(res =>{})  
      .catch(error => Observable.throw(error))  
      .subscribe(  
      data => console.log('success'),  
      error => console.log(error))  
    }
    window.location.reload();  
  }

}
