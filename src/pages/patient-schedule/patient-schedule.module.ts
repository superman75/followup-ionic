import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientSchedulePage } from './patient-schedule';
import { ComponentsModule } from '../../components/components.module';
import { CalendarModule } from "ion2-calendar";
@NgModule({
  declarations: [
    PatientSchedulePage,
  ],
  imports: [
    ComponentsModule,
    CalendarModule,
    IonicPageModule.forChild(PatientSchedulePage),
  ],
})
export class PatientSchedulePageModule {}
