import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HP,Appointments,BusyDates,dayConfig } from '../../providers/modals/modals';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import { CalendarComponentOptions,DayConfig } from 'ion2-calendar'

import {ToastProvider } from '../../providers/toast/toast';


/**
 * Generated class for the PatientSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patient-schedule',
  templateUrl: 'patient-schedule.html',
})
export class PatientSchedulePage {

  hplist: HP[] = [];
  selectedHP: HP;
  selectedHPFrmResult: HP;
  date: any;
  appointmentDetails:Appointments = new Appointments();
  notes:string = "";
  timeSlotIndex:string = "1";
  timeslotValue:string = "09am - 12pm";
  calendarOption: CalendarComponentOptions = { pickMode: 'single', showToggleButtons: true};
  patientAppointments: Appointments[]=[];
  _daysConfig: DayConfig[] = [];
  tempConfig:dayConfig= new dayConfig();
  busyDates:BusyDates = new BusyDates();

  constructor(public toastProvider: ToastProvider,public navCtrl: NavController, public navParams: NavParams, public serviceHelper: ServiceHelperProvider ) {
    this.LoadHPList();
     // this.appointmentDetails = new Appointments();
     this.appointmentDetails.Notes = "";
     this.appointmentDetails.TimeBlock = this.GetSlotText("1");
     this.LoadAllAppointments();
    // this.LoadBusyDates();
    this.selectedHP=new HP();
    
    console.log(this.selectedHPFrmResult);
    //selectedHP
  }

  
  LoadBusyDates(providerId)
  {
    this.serviceHelper.getRequest("api/Appointment/GetBusyDatesForHP?userId="+providerId)
    .subscribe(
        data => {
         this.busyDates = <BusyDates> data;
         if (this.busyDates.BusyDate!=null) {
          this.busyDates.BusyDate.forEach((item) => { // foreach statement  
            this.tempConfig= new dayConfig();
            this.tempConfig.date = item;
            this.tempConfig.cssClass='dateunavailable';
  
            this._daysConfig.push(this.tempConfig);
         }) 
         this.calendarOption = { pickMode: 'single', showToggleButtons: true,daysConfig:this._daysConfig};
         }
        
      
        },
        error => {
        // this.toastProvider.presentToast("Service Unavailable. Please try after some time.");
        });

  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientSchedulePage');
  }

  cancelClick()
  {
      this.navCtrl.pop();
  }

  onChange($event) {
    console.log(this.date);
  }

  LoadAllAppointments()
  {

    this.serviceHelper.getRequest("api/Appointment/GetPatientAppointment")
    .subscribe(
        data => {
          this.patientAppointments = <Appointments[]> data;
          console.log(this.patientAppointments);
        },
        error => {
         console.log("In Error");
        });

  }

  DeleteAppointment(item)
  {

    this.serviceHelper.PostRequest("api/Appointment/DeleteAppointment",item)
    .subscribe(
        data => {
         console.log(data);
         this.LoadAllAppointments();
         this.toastProvider.presentToast("Appointment Deleted successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while deleting appointment, Please try again later.");
        });
  }

  UpdateUrgetStatus(item)
  {

    this.serviceHelper.PostRequest("api/Appointment/ManageUrgentAppointment",item)
    .subscribe(
        data => {
         console.log(data);
         this.toastProvider.presentToast("Appointment updated successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while booking appointment, Please try again later.");
        });

  }


  LoadHPList()
  {
    
    this.serviceHelper.getRequest("api/Appointment/GetHPList")
    .subscribe(
        data => {
          console.log(data);
          this.hplist =<HP[]> data;
          try
          {
            this.selectedHPFrmResult= this.navParams.get('selectedHP');
            if (this.selectedHPFrmResult==null) {
              this.selectedHP = this.hplist[0];
              
            }else{
             this.selectedHP=this.hplist.find(x => x.ProviderGuid === this.selectedHPFrmResult.ProviderGuid);
            }
         
          this.LoadBusyDates(this.selectedHP.ProviderID)
          console.log(this.selectedHP);
          }
          catch(Exception)
          {}
        },
        error => {
         console.log("In Error");
        });
        
        
  }

  selectSlot(index)
  {
      this.timeSlotIndex = index;
      this.timeslotValue = this.GetSlotText(index);
      this.appointmentDetails.TimeBlock = this.GetSlotText(index);
      //console.log( this.timeslotValue);
  }

  onHPSelect(CValue) {
    console.log(CValue);
    try
    {
    this.LoadBusyDates(this.selectedHP.ProviderID)
    //console.log(this.selectedHP);
    }
    catch(Exception)
    {}
}
  


   GetSlotText(index):string 
   {
     var initVal = "09am - 12pm";
      switch(index)
      {
        case "1":
          return "09am - 12pm";
        case "2":
        return "12pm - 14pm";
        case "3":
        return "14pm - 16pm";
        case "4":
        return "16pm - 18pm";
        case "5":
        return "18pm - 20pm";
        case "6":
        return "20pm - 22 pm";
      }
      return initVal;
   }

   BookAppoinmtment()
   {
    this.appointmentDetails.ProviderID = this.selectedHP.ProviderID;
    this.appointmentDetails.Date = new Date(this.date.toDate());
    this.appointmentDetails.Date.setDate( this.appointmentDetails.Date.getDate() + 1 );
    this.appointmentDetails.Notes = this.notes;
    console.log(this.appointmentDetails);

    this.serviceHelper.PostRequest("api/Appointment/BookAppointment", this.appointmentDetails)
    .subscribe(
        data => {
         this.appointmentDetails.AppointmentID = <number> data; 
         this.appointmentDetails.Notes ="";
         this.notes = "";
         this.LoadAllAppointments();
         this.toastProvider.presentToast("Appointment booked successfully.");
         console.log(data);
        },
        error => {
          this.toastProvider.presentToast("Error while booking appointment, Please try again.");
         console.log("In Error");
        });

   }


}
