import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import{SessionManagerProvider} from '../../providers/session-manager/session-manager';
import {ChangePasswordModel} from '../../providers/modals/modals';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  isPatient:boolean;
  userName:string;
  changePasswordObj:ChangePasswordModel = new ChangePasswordModel();
  constructor(public navCtrl: NavController, public navParams: NavParams,public serviceHelperProvider: ServiceHelperProvider,public sessionManagerProvider:SessionManagerProvider) {
      this.userName = this.sessionManagerProvider.getName();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }


  ChangePassword(){
    this.isPatient=this.sessionManagerProvider.getPatient();
    this.changePasswordObj.OldPassword = this.sessionManagerProvider.getTempPassword();
    //console.log('acceptServiceAgmnt');
    this.serviceHelperProvider.PostRequest("api/Account/ChangePassword", this.changePasswordObj).subscribe(
        data => {

          this.sessionManagerProvider.setisTempPassword("False");
          
          if (this.sessionManagerProvider.getShowAssessmentReminder()) 
          {
            this.navCtrl.push('AssessmentReminderPage',{selectedAssgnmnt:this.sessionManagerProvider.getRemiderAssessmentDetail()});
          } else {
            if(this.isPatient){
              this.navCtrl.push('PatientDashboardPage');
            }else{
              this.navCtrl.push('HpDashboardPage');
            }
          }

         
         console.log(data);
        },
        error => {
         console.log("In Error");
        });


        

  }


}
