import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutPage } from './about';
// import { HptabsComponent } from '../../components/hptabs/hptabs';
// import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AboutPage,
    // HptabsComponent,
    // TopNavbarComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AboutPage),
    
  ],
})
export class AboutPageModule {}
