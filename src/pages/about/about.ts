import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  isPatient:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,public sessionManagerProvider:SessionManagerProvider) {
    
    this.isPatient=sessionManagerProvider.getPatient();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

}
