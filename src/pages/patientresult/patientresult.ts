import { Component, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PatientResult,Gender,PatientAssessmentInfo,PatientAssessmentResult,HP } from '../../providers/modals/modals';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';


/**
 * Generated class for the PatientresultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patientresult',
  templateUrl: 'patientresult.html',
})
export class PatientresultPage {
  abvAvg:boolean=true;
  viewResult:boolean=false;
  patientAssessmentResultArray:PatientAssessmentResult[]=[];
  patientAssessmentInfo:PatientAssessmentInfo[]=[];
  hp:HP;
  constructor(public navCtrl: NavController, public navParams: NavParams,public serviceHelperProvider:ServiceHelperProvider) {
    this.loadPatientAssessment();
    this.loadAssessmentResult();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientresultPage');
  }
  test(){
    console.log('test');
  }
  viewPatientResultDetail(item:any){
    console.log('viewPatientResultDetail');

    item.ViewResultDetail= !item.ViewResultDetail;
  }
  loadAssessmentResult(){
    
      
          this.serviceHelperProvider.getRequest("api/Assessment/GetPatientAssessResultInfo")
          .subscribe(
              data => {
                
                console.log(data);
                this.patientAssessmentResultArray= <PatientAssessmentResult[]>data;
                 console.log(data);
              },
              error => {
               console.log(error);
              });
    }
    bookVisit(item:PatientAssessmentResult){
      this.hp=new HP();
      this.hp.Name= item.ProviderName;
      this.hp.ProviderGuid=item.ProviderGuid;
      
      console.log('bookVisit');
      this.navCtrl.push('PatientSchedulePage',{  selectedTab:"schedule", selectedHP:this.hp });
      

    }
  sendMessage(item:PatientAssessmentResult){
    console.log('sendMessage');
    this.navCtrl.push('MessagePage');
  }
  loadPatientAssessment() {
    
        this.serviceHelperProvider.getRequest("api/Assessment/GetPatientAssessmentList")
        .subscribe(
            data => {
              
              console.log(data);
              this.patientAssessmentInfo= <PatientAssessmentInfo[]>data;
              
              console.log(data);
            },
            error => {
             console.log(error);
            });
  }
  startQuiz(item:any){
    console.log(item)
 //go to start quiz page
 this.navCtrl.push('StartQuizPage',{selectedAssgnmnt:item});
  }

  

}
