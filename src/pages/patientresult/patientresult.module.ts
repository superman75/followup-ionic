import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientresultPage } from './patientresult';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PatientresultPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PatientresultPage),
  ],
})
export class PatientresultPageModule {}
