import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';
import { Renderer } from '@angular/core';
import {Assessment, QuestionAnswer, Answer, ScoreRange, AnswersLookup,AnswersLookupNew, AssessmentInfo, PatientBasicInfo, LookupData, Category, CategoryList, ResultRange} from '../../providers/modals/modals'
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper'
import { ModalController } from 'ionic-angular';
/**
 * Generated class for the AssessmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assessments',
  templateUrl: 'assessments.html',
})
export class AssessmentsPage {
  existingAssessments: AssessmentInfo[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public serviceProvider: ServiceHelperProvider,public modalCtrl : ModalController) {
    this.getProviderAssessments();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssessmentsPage');
  }

  getProviderAssessments(){
    this.serviceProvider.getRequest("api/Assessment/GetProviderAssessmentList")
    .subscribe(
    (data: AssessmentInfo[]) => {
      console.log(data);
      this.existingAssessments = data;
    },
    error => {
      console.log(error);
      console.log("In Error");
    });
  }

 editAssessment(assessmentGuid: string)
  {
    console.log(assessmentGuid);
      this.serviceProvider.getRequest("api/Assessment/GetAssessment?assessmentGuid=" + assessmentGuid)
      .subscribe(
      (data: Assessment) => {
        console.log('Assesment Data ' + data);
        console.log( data);
        if(data != null && data.Quiz != null && data.Quiz.length > 0)
          {
            data.Quiz.forEach(q => {

              var AnswersLookups = AnswersLookupNew;
              q.AnswersLookup =  JSON.parse(JSON.stringify(AnswersLookups));
              if(q.Answers != null && q.Answers.length > 0)
                {
                  q.AnswerString = [];
                  q.Answers.forEach(a => {
                      q.AnswerString.push(a.Code);
                        q.AnswersLookup.forEach(b => {
                          if(b.Code == a.Code)
                          {
                            b.Checked = true;
                            b.Score = a.Score;
                          }
                      });
                  });

                 
                }
            });
          }
        this.serviceProvider.setSelectedAssessment(data);        
        //var modalPage = this.modalCtrl.create('CreateAssignmentPage'); 
        var modalPage = this.modalCtrl.create('CreateAssessmentPage'); 
        
        modalPage.present();
      },
      error => {
        console.log(error);
        console.log("In Error");
      });
  }

}
