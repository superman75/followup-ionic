import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssessmentsPage } from './assessments';
// import { HptabsComponent } from '../../components/hptabs/hptabs';
// import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AssessmentsPage,
    // HptabsComponent,
    // TopNavbarComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AssessmentsPage),
  ],
})
export class AssessmentsPageModule {}
