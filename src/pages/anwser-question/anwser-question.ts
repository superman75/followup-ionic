import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import { ToastProvider } from '../../providers/toast/toast';
import { PatientResult, Gender, PatientResultView, Assessment, QuestionAnswer, PatientAssessmentInfo, AssessmentQuestions, ScoreRange, Answer, QuestionAnswerQuiz } from '../../providers/modals/modals';
import { concat } from 'rxjs/operator/concat';


/**
 * Generated class for the AnwserQuestionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-anwser-question',
  templateUrl: 'anwser-question.html',
})
export class AnwserQuestionPage {
  url: string;
  assessmentDetail: AssessmentQuestions = new AssessmentQuestions();
  questionLength: number;
  quiz: QuestionAnswerQuiz[] = [];
  assessmentInfo: PatientAssessmentInfo;
  assnmntGUID: string;
  selectedQuestionIndex: number = 0;
  score;
  resultRange: ScoreRange[] = [];
  slectedQuestionsAnswers: Answer[] = [];
  scoreArray: number[] = [];
  minScore: number = 0;
  maxScore: number;
  selectedScore: number;
  minScoreText: string;
  maxScoreText: string;
  selectedScoreText: string;
  quizFromNav: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public serviceHelperProvider: ServiceHelperProvider, public toastProvider: ToastProvider) {
    this.quizFromNav = this.navParams.get('quiz');
    this.assessmentInfo = this.navParams.get('selectedAssgnmnt');
    // this.assnmntGUID = "38f7de90-524e-41c7-8df5-08b8f4b206dc";
    this.assnmntGUID = this.assessmentInfo.AssessmentGuid;
    console.log(this.assnmntGUID);
    if (this.quizFromNav === undefined) {
      this.loadAssignemntDetail();
    } else {
      this.selectedQuestionIndex = 0;
      this.quiz = this.navParams.get('quiz');
      this.assessmentDetail = this.navParams.get('assessmentDetail');
      this.getMinMaxScore();
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AnwserQuestionPage');

  }
  loadAssignemntDetail() {
    this.url = 'api/Assessment/GetAssessment?assessmentGuid=' + this.assnmntGUID;



    this.serviceHelperProvider.getRequest(this.url)
      .subscribe(
      (data: AssessmentQuestions) => {
        this.assessmentDetail = data;
        this.quiz = this.assessmentDetail.Quiz;
        this.getMinMaxScore();


      },
      error => {
        console.log("In Error");
        this.toastProvider.presentToast("Error while fetching assessment info, Please try again later.");
      });
  }
  getMinMaxScore() {

    this.slectedQuestionsAnswers = this.assessmentDetail.Quiz[this.selectedQuestionIndex].Answers;
    this.slectedQuestionsAnswers = this.slectedQuestionsAnswers.sort(function (obj1, obj2) {
      return obj1.Score - obj2.Score;
    });
    this.minScore = this.slectedQuestionsAnswers[0].Score;
    this.maxScore = this.slectedQuestionsAnswers[this.slectedQuestionsAnswers.length - 1].Score;
    this.selectedScore = this.minScore;

    this.minScoreText = this.slectedQuestionsAnswers[0].Text;
    this.maxScoreText = this.slectedQuestionsAnswers[this.slectedQuestionsAnswers.length - 1].Text;
  }
  goToNext(item, i) {
    //set answered value for question
    this.slectedQuestionsAnswers.forEach(element => {
      if (element.Score == this.selectedScore) {
        this.quiz[this.selectedQuestionIndex].AnswerString = element.Code;
      }
    })
    if (this.selectedQuestionIndex < (this.quiz.length - 1)) {

      this.selectedQuestionIndex++;
      this.getMinMaxScore();
    }
    else {
      this.navCtrl.push('SubmitQuizPage', { selectedAssgnmnt: this.assessmentInfo, quiz: this.quiz, assessmentDetail: this.assessmentDetail });
    }
  }



  goToBack(item, i) {
    if (this.selectedQuestionIndex > 0) {
      this.selectedQuestionIndex--;

    } else {

      this.navCtrl.push('StartQuizPage', { selectedAssgnmnt: this.assessmentInfo });
    }
  }
  onSliderChanged(selectedScore: number) {
    this.slectedQuestionsAnswers.forEach(element => {
      if (element.Score == selectedScore) {
        this.selectedScoreText = element.Text;
      }

    });

  }
  onionSelect(selectedScore: number){
    console.log('onionSelect');
    this.slectedQuestionsAnswers.forEach(element => {
      if (element.Score == selectedScore) {
        this.selectedScoreText = element.Text;
      }

    });
  }
}
