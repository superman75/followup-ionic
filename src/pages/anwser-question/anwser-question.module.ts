import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnwserQuestionPage } from './anwser-question';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AnwserQuestionPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AnwserQuestionPage),
  ],
})
export class AnwserQuestionPageModule {}
