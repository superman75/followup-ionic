import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitQuizPage } from './submit-quiz';
import { NavBarQuizComponent } from '../../components/nav-bar-quiz/nav-bar-quiz';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    SubmitQuizPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SubmitQuizPage),
  ],
})
export class SubmitQuizPageModule {}
