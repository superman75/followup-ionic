import { Component, transition } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NavBarQuizComponent } from '../../components/nav-bar-quiz/nav-bar-quiz';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';
import { PatientResult, Gender, PatientResultView, Assessment, QuestionAnswerQuiz, PatientAssessmentInfo, AssessmentQuestions,ScoreRange,SavePatient,QuestionAnswerDetails} from '../../providers/modals/modals';
import {ToastProvider } from '../../providers/toast/toast';


/**
 * Generated class for the SubmitQuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-submit-quiz',
  templateUrl: 'submit-quiz.html',
})
export class SubmitQuizPage {
  isPatient: boolean = false;
  assessmentInfo: PatientAssessmentInfo;
  isCombined:boolean=true;
  quiz: QuestionAnswerQuiz[] = [];
  savePatient:SavePatient;
  questionAnswerDetails:QuestionAnswerDetails[]=[];
  questionAnswerDetailsObj:QuestionAnswerDetails;
  assessmentDetail: AssessmentQuestions=new AssessmentQuestions();
  providerImage:string="";
  isProviderImage:boolean;
  constructor(public sessionManagerProvider:SessionManagerProvider,public navCtrl: NavController, public navParams: NavParams,public serviceHelperProvider:ServiceHelperProvider,public toastProvider:ToastProvider) {
    this.assessmentInfo = this.navParams.get('selectedAssgnmnt');
   // this.assessmentInfo.ProviderName
    this.isCombined=true;
    this.savePatient=new SavePatient();
    this.assessmentDetail= this.navParams.get('assessmentDetail');
    this.isPatient = this.sessionManagerProvider.getPatient();
    this.quiz = this.navParams.get('quiz');
    this.getProviderImage(this.assessmentInfo.ProviderGuid);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubmitQuizPage');
  }
  submitQuiz(){
    if(!this.isPatient)
      return;

    this.questionAnswerDetails=[];
    this.savePatient.AssessmentGuid=this.assessmentInfo.AssessmentGuid;
    this.savePatient.UserGuid="";
    this.quiz.forEach(element => {
      this.questionAnswerDetailsObj=new QuestionAnswerDetails();
      this.questionAnswerDetailsObj.key=element.QuestionGuid;
      this.questionAnswerDetailsObj.value=element.AnswerString;
      this.questionAnswerDetails.push(this.questionAnswerDetailsObj);
      
    });
    this.savePatient.QuestionAnswer=this.questionAnswerDetails;
   //temp hack
    this.savePatient.UserIntID=4;
    this.serviceHelperProvider.PostRequest("api/Assessment/SaveForPatient",this.savePatient)
    .subscribe(
        data => {
          debugger;
          this.toastProvider.presentToast("Submited Quiz Successfully.");
          this.sessionManagerProvider.setShowAssessmentReminder(false);
          this.sessionManagerProvider.setRemiderAssessmentDetail(undefined);
          this.navCtrl.push('PatientDashboardPage');
           
        },
        error => {
          this.toastProvider.presentToast("Error While Submitting Quiz.");
         console.log(error);
        });
  }

  goToQuiz(){
    if(!this.isPatient)
      return;
        this.isCombined=false;
        this.navCtrl.push('AnwserQuestionPage',{selectedAssgnmnt:this.assessmentInfo,quiz:this.quiz,assessmentDetail:this.assessmentDetail});
  }
  getProviderImage(userGuid:string){
    this.serviceHelperProvider.PostRequest("api/Message/GetUserProfileImage?userGuid="+userGuid,"")
    .subscribe(
    data => {
      if (data=="") {
        this.isProviderImage=false;
      }else{
        this.isProviderImage=true;
        this.providerImage="data:image/png;base64,"+data;
      }
    },
    error => {
        console.log("In Error");
    });
  }
}
