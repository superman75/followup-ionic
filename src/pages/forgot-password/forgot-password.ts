import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {ForgotPasswordModel} from '../../providers/modals/modals';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  showForgotForm:boolean=true;
  forgotPassword:ForgotPasswordModel= new ForgotPasswordModel();
  constructor(public navCtrl: NavController, public navParams: NavParams,public serviceHelperProvider:ServiceHelperProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }
  goToSignUp(){
    console.log('Click goToSignUp!');
    this.navCtrl.push('SignUpPage');
  }
  continue(){

    this.serviceHelperProvider.PostRequestWithoutHeader("api/Account/ForgotPassword", this.forgotPassword).subscribe(
      data => {
      this.showForgotForm=false;
       console.log(data);
      },
      error => {
        this.showForgotForm=false;
       console.log("In Error");
      });

   
  }
  goToSignIN(){
    this.navCtrl.popToRoot();
  }

  

}
