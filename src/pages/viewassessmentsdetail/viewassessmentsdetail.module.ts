import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewassessmentsdetailPage } from './viewassessmentsdetail';
// import { HptabsComponent } from '../../components/hptabs/hptabs';
// import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ViewassessmentsdetailPage,
    // HptabsComponent,
    // TopNavbarComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ViewassessmentsdetailPage),
  ],
})
export class ViewassessmentsdetailPageModule {}
