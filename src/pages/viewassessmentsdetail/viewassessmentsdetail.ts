import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HptabsComponent } from '../../components/hptabs/hptabs';
import { TopNavbarComponent } from '../../components/top-navbar/top-navbar';


/**
 * Generated class for the ViewassessmentsdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewassessmentsdetail',
  templateUrl: 'viewassessmentsdetail.html',
})
export class ViewassessmentsdetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewassessmentsdetailPage');
  }

}
