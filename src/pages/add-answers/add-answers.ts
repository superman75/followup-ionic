import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {PatientBasicInfo, Answer, AnswersLookup, AnswerValue} from '../../providers/modals/modals';

/**
 * Generated class for the AddHealthcarePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-answers',
  templateUrl: 'add-answers.html',
})
export class AddAnswersPage {

  providerCode: string = "";
  answers: Answer[] = AnswersLookup;
  answerScores: number[] = new Array<number>(this.answers.length);
  answerChecks: boolean[] = new Array<boolean>(this.answers.length);
  questionAnswers: Answer[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
    public renderer: Renderer, public serviceProvider: ServiceHelperProvider) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup2', true);
    this.questionAnswers = this.navParams.get('questionAnswerList');

    this.questionAnswers.forEach(element => {
      let index = this.answers.findIndex(a => a.Code == element.Code);
      this.answerChecks[index] = true;
      this.answerScores[index] = element.Score;

    });
    console.log(this.answerChecks);
    console.log(this.answerScores);
    console.log(this.questionAnswers);
    
  }

  ionViewDidLoad() {
      console.log(this.navParams);
      console.log('ionViewDidLoad AddAnswersPage');
  }
  closeModal(){
    console.log('close modal start');
    let index = 0;
    this.answers.forEach(a => {
        for(var i =0; i < this.questionAnswers.length; i++)
          {
          if(this.questionAnswers[i].Code == a.Code)
            {
                this.questionAnswers[i].Score = this.answerScores[index];
                break;
            }
        }
        index = index + 1;       
    });

    console.log(this.questionAnswers);
    console.log('close modal end');
    this.viewCtrl.dismiss(this.questionAnswers);
  }

  updateAnswer(answer: Answer, e: any, i: number)
  {
    console.log(answer);
    console.log(e.checked);
    
    if(e.checked)
    {
        let newAnswer = new Answer();
        newAnswer.Code = answer.Code;
        newAnswer.Text = answer.Text;
        if(this.answerScores[i] == undefined)
          {
            newAnswer.Score = 0;
            this.answerScores[i] = 0;
          }
        else
          newAnswer.Score = this.answerScores[i];

        this.questionAnswers.push(newAnswer);
    }
    else
    {
        let index = this.questionAnswers.findIndex(a => a.Code === answer.Code ); 
        this.questionAnswers.splice(index, 1);   
     }
      console.log(this.questionAnswers);
  }

}
