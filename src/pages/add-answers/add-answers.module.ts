import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAnswersPage } from './add-answers';

@NgModule({
  declarations: [
    AddAnswersPage,
  ],
  imports: [
    IonicPageModule.forChild(AddAnswersPage),
  ],
})
export class AddAnswersPageModule {}
