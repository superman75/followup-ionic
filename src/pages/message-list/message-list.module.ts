import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageListPage } from './message-list';
import { ChatServiceProvider } from "../../providers/chat-service/chat-service";
import { RelativeTimePipe } from "../../pipes/relative-time/relative-time";
//import { RelativeTime } from "../../pipes/relative-time";
//import { EmojiPickerComponentModule } from "../../components/emoji-picker/emoji-picker.module";
//import { EmojiProvider } from "../../providers/emoji";

@NgModule({
  declarations: [
    MessageListPage,
    RelativeTimePipe
  ],
  imports: [
    IonicPageModule.forChild(MessageListPage),
  ],
})
export class MessageListPageModule {}





