import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content} from 'ionic-angular';
import { ChatContact, ChatModel, ChatRequest, AddChatModal } from '../../providers/modals/modals';
import { ChatServiceProvider, ChatMessage, UserInfo } from "../../providers/chat-service/chat-service";
import { concat } from 'rxjs/operators/concat';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import {SessionManagerProvider} from '../../providers/session-manager/session-manager';

/**
 * Generated class for the MessageListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message-list',
  templateUrl: 'message-list.html',
})
export class MessageListPage {

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;

  user: UserInfo;
  toUser: UserInfo;
  editorMsg = '';
  showEmojiPicker = false;

  toUserInfo: ChatContact;
  msgList: ChatModel[] = [];
  chatRequest: ChatRequest = new ChatRequest();
  addChat: AddChatModal = new AddChatModal();
  chatMessage: ChatModel = new ChatModel();

  currentUserImage:string ="";
  toUserImage:string="";
  isPatient:boolean = false;

  constructor(public sessionManagerProvider:SessionManagerProvider,private navParams: NavParams,
      private chatService: ChatServiceProvider,
              public serviceHelper: ServiceHelperProvider,
              private events: Events) {
                this.toUserImage = this.sessionManagerProvider.getToUserImage();
                var temp =  this.sessionManagerProvider.getUserImage();
                if(temp == null)
                {
                  this.currentUserImage = null;
                }
                else
                {
                  this.currentUserImage = "data:image/png;base64," + temp;
                }
                this.isPatient = this.sessionManagerProvider.getPatient();
                //console.log();
    // Get the navParams toUserId parameter
    // this.toUser = {
    //   id: navParams.get('toUserId'),
    //   name: navParams.get('toUserName')
    // };
    /*this.toUser = {
      id:'210000198410281948',
      name:'Hancock'
    }
    console.log(this.toUser);
    // Get mock user information
    this.chatService.getUserInfo()
    .then((res) => {
      this.user = res
    });*/

      this.toUserInfo = <ChatContact> JSON.parse(this.navParams.get("jsonData"));

  }

  ionViewWillLeave() {
    // unsubscribe
    this.events.unsubscribe('chat:received');
  }

  ionViewDidEnter() {
    //get message list
    this.getMsg();

    // Subscribe to received  new message events
   /* this.events.subscribe('chat:received', msg => {
      //this.pushNewMsg(msg);
    })*/
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  getMsg() {
      this.chatRequest.userGuid = this.toUserInfo.UserGuid;
      this.chatRequest.PageIndex = this.chatRequest.PageIndex++;
      this.chatRequest.PageSize = 400;
      this.serviceHelper.PostRequest("api/Message/GetChatMessages",this.chatRequest)
          .subscribe(
          data => {
              this.msgList = <ChatModel[]>data;
              this.scrollToBottom();
              try {
                  this.msgList.forEach((item) => { // foreach statement  
                      if (item.FromUser) {
                          this.chatMessage.UserName = item.UserName;
                      }
                      //break;
                      return;
                  })
              }
              catch (Exception) {

              } 
          },
          error => {
              console.log("In Error");
          });

    // Get mock message list
    /*return this.chatService
    .getMsgList()
    .subscribe(res => {
      this.msgList = res;
      this.scrollToBottom();
    });*/
  }

  /**
   * @name sendMsg
   */

  sendMessage() {
      this.addChat.UserGuid = this.toUserInfo.UserGuid;
      this.addChat.Message = this.editorMsg;

      var userName = this.chatMessage.UserName;
      var date = new Date();
      this.chatMessage = new ChatModel();
      this.chatMessage.Message = this.editorMsg;
      this.chatMessage.MessageDate = new Date();
      this.chatMessage.FromUser = true;
      this.chatMessage.UserName = userName;
      if (!(this.msgList))
          this.msgList = [];
      this.msgList.push(this.chatMessage);
      this.scrollToBottom();
      this.serviceHelper.PostRequestWOLoader("api/Message/AddChatMessage", this.addChat)
          .subscribe(
          data => {
              this.addChat.Message = "";
              this.editorMsg = "";
              //console.log(this.msgList);
          },
          error => {
              console.log("In Error");
          });
  }
    /*
  sendMsg() {
    if (!this.editorMsg.trim()) return;

    // Mock message
    const id = Date.now().toString();
    let newMsg: ChatMessage = {
      messageId: Date.now().toString(),
      userId: this.user.id,
      userName: this.user.name,
      userAvatar: this.user.avatar,
      toUserId: this.toUser.id,
      time: Date.now(),
      message: this.editorMsg,
      status: 'pending'
    };

    this.pushNewMsg(newMsg);
    this.editorMsg = '';

    if (!this.showEmojiPicker) {
      this.focus();
    }

   /* this.chatService.sendMsg(newMsg)
    .then(() => {
      let index = this.getMsgIndexById(id);
      if (index !== -1) {
        this.msgList[index].status = 'success';
      }
    })*/
 /* }*/

  /**
   * @name pushNewMsg
   * @param msg
   */
      /*
  pushNewMsg(msg: ChatMessage) {
    const userId = this.user.id,
      toUserId = this.toUser.id;
    // Verify user relationships
    if (msg.userId === userId && msg.toUserId === toUserId) {
      this.msgList.push(msg);
    } else if (msg.toUserId === userId && msg.userId === toUserId) {
      this.msgList.push(msg);
    }
    this.scrollToBottom();
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id)
  }
 */

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea =this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }

}
