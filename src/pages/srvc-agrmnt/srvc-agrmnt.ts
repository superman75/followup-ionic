import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
 import{SessionManagerProvider} from '../../providers/session-manager/session-manager';
import { CalendarResult } from 'ion2-calendar';

/**
 * Generated class for the SrvcAgrmntPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-srvc-agrmnt',
  templateUrl: 'srvc-agrmnt.html',
})
export class SrvcAgrmntPage {
  isPatient:boolean;
  isTempPAssword:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,public serviceProvider:ServiceHelperProvider,public sessionManagerProvider:SessionManagerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SrvcAgrmntPage');
  }
  acceptServiceAgmnt(){
    this.isPatient=this.sessionManagerProvider.getPatient();
    this.isTempPAssword = this.sessionManagerProvider.getisTempPassword();
    //console.log('acceptServiceAgmnt');
    this.serviceProvider.PostRequest("api/Account/AcceptServiceAgreement", '').subscribe(
        data => {
          if(this.isTempPAssword == "True")
          {
           // this.sessionManagerProvider.setisTempPassword("True");
            this.navCtrl.push('ChangePasswordPage');
          }
          else
          {
            if (this.sessionManagerProvider.getShowAssessmentReminder()) 
            {
              this.navCtrl.push('AssessmentReminderPage',{selectedAssgnmnt:this.sessionManagerProvider.getRemiderAssessmentDetail()});
            } else {
              if(this.isPatient){
                this.navCtrl.push('PatientDashboardPage');
              }else{
                this.navCtrl.push('HpDashboardPage');
              }
            }
            
        }
         console.log(data);
        },
        error => {
         console.log("In Error");
        });


        

  }
}


