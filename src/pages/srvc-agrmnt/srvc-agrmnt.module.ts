import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SrvcAgrmntPage } from './srvc-agrmnt';


@NgModule({
  declarations: [
    SrvcAgrmntPage,
  ],
  imports: [
    IonicPageModule.forChild(SrvcAgrmntPage),
  ],
})
export class SrvcAgrmntPageModule {}
