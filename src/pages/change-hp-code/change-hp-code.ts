import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import {ToastProvider} from '../../providers/toast/toast'
import {PatientBasicInfo} from '../../providers/modals/modals';
import { Http, RequestOptions, Headers, Response } from '@angular/http';  
import { Observable } from 'rxjs/Rx';
import { HttpModule } from '@angular/http';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';



/**
 * Generated class for the ChangeHpCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-hp-code',
  templateUrl: 'change-hp-code.html',
})
export class ChangeHpCodePage {

  providerCode: string = "";
  constructor(public http: HttpClient,public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController,
    public renderer: Renderer, public serviceProvider: ServiceHelperProvider,public toastProvider:ToastProvider) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup1', true);
    this.getProviderCode();
  }

  getProviderCode(){
    
    this.serviceProvider.getRequest("api/Assessment/GetUserCode")
    .subscribe(
    (data ) => {
      console.log(data);
      this.providerCode=data;
    },
    error => {
    this.toastProvider.presentToast("Error while getting personal code.")
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddHealthcarePage');
  }
  closeModal(){
    console.log('close');
    this.viewCtrl.dismiss();
  }
  UpdateCode(){
    this.serviceProvider.PostRequest("api/Assessment/EditUserCode?userCode="+this.providerCode,{})
    .subscribe(
    (data ) => {
      console.log(data);
      
    },
    error => {
      console.log(error);
      
    });
  }
 
  

}
