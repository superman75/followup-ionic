import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeHpCodePage } from './change-hp-code';

@NgModule({
  declarations: [
    ChangeHpCodePage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeHpCodePage),
  ],
})
export class ChangeHpCodePageModule {}
