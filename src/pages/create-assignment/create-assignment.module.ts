import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAssignmentPage } from './create-assignment';

@NgModule({
  declarations: [
    CreateAssignmentPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateAssignmentPage),
  ],
})
export class CreateAssignmentPageModule {}
