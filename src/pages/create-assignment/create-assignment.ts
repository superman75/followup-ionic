import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';
import { Renderer } from '@angular/core';
import {Assessment, QuestionAnswer, Answer, ScoreRange, AnswersLookup, AssessmentInfo, PatientBasicInfo, LookupData, Category, CategoryList, ResultRange} from '../../providers/modals/modals'
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper'
import { ModalController } from 'ionic-angular';
import {ToastProvider} from '../../providers/toast/toast'

/**
 * Generated class for the CreateAssignmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-assignment',
  templateUrl: 'create-assignment.html',
})
export class CreateAssignmentPage {

  assessment: Assessment = null;
  answers: Answer[] = AnswersLookup;
  categories: Category[] = CategoryList;
  subcategories: LookupData[];
  resultRanges: LookupData[] = ResultRange;
  patientList: PatientBasicInfo[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer: Renderer,
    public viewCtrl: ViewController, public serviceProvider: ServiceHelperProvider,public modalCtrl : ModalController,public toastProvider:ToastProvider) {
    
    this.assessment = this.serviceProvider.getSelectedAssessment();
    
    if(this.assessment == undefined || this.assessment == null)
      {
        this.assessment = new Assessment();
        this.assessment.OperationType = 1;
        this.assessment.Quiz = [];
        this.addNewQuestion();
        this.assessment.ResultRange = [];
        this.addResultRange();
        this.assessment.Details = "";
      }
      else
        {
          this.assessment.OperationType = 2;
          this.assessment.Quiz.forEach(e => {
            e.OperationType = 2;
          })
        }

    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup-assignment', true);
    this.LoadPatients();
  }

  addResultRange()
  {
    var range = new ScoreRange();
    range.MinScore = 0;
    range.MaxScore = 0;
    range.ResultText = "";

    this.assessment.ResultRange.push(range);
  }

  addNewQuestion()
  {
    var question1 = new QuestionAnswer();
    question1.AnswerString = [];
    question1.Question = "";
    question1.OperationType = 1;
    question1.Answers = [];
    this.assessment.Quiz.push(question1);
  }

  showSubcategories(categoryCd: string)
  {
      let selectedCategory = this.categories.find(c => c.Key === categoryCd)
      if(selectedCategory && selectedCategory.SubCategories && selectedCategory.SubCategories.length > 0)
        this.subcategories = selectedCategory.SubCategories;
      else
        this.subcategories = [];

      this.assessment.SubCategoryCd = "";
  }

    getCategory(categoryCd: string): string
  {
      let selectedCategory = this.categories.find(c => c.Key === categoryCd)
      if(selectedCategory && selectedCategory.SubCategories && selectedCategory.SubCategories.length > 0)
        return selectedCategory.Value;
      else
        return "Category";
  }

  LoadPatients() {
    this.serviceProvider.getRequest("api/Assessment/GetPatients?searchString=&existingPatientsOnly=true")
    .subscribe(
        (data: PatientBasicInfo[]) => {
          console.log(data);
          this.patientList = data;
        },
        error => {
         console.log("In Error");
        });
  }

  showAnswerList(quiz: QuestionAnswer)
  {
    quiz.Answers = [];
    if(quiz.AnswerString != null && quiz.AnswerString.length > 0)
      {
        quiz.AnswerString.forEach(element => {
            let answer = new Answer();
            var lookupAnswer = AnswersLookup.find(a => a.Code == element);
            answer.Code = lookupAnswer.Code;
            answer.Text = lookupAnswer.Text;
            answer.Score = lookupAnswer.Score;
            quiz.Answers.push(answer);
        });
        

      }
  }

  getAssessment(assessmentGuid: string)
  {
      this.serviceProvider.getRequest("api/Assessment/GetAssessment?assessmentGuid=" + assessmentGuid)
      .subscribe(
      (data: Assessment) => {
        console.log(data);
        this.assessment = data;
        this.assessment.OperationType = 2;
      },
      error => {
        console.log(error);
        console.log("In Error");
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateAssignmentPage');
  }
closeModal(){
  
  this.serviceProvider.setSelectedAssessment(null);
    console.log('close');
    this.viewCtrl.dismiss();
  }

  onAnswerSelect(){
    console.log(this.assessment);
  }

  createAssessment(){
    console.log(this.assessment);

    if(!this.assessment.Title || this.assessment.Title == null)
      {
        this.assessment.Title="This is default assessment"
        //this.closeModal();
        // this.modalCtrl.create({ dismiss: true });      
      }
      else
        {


        this.serviceProvider.PostRequest("api/Assessment/Manage", this.assessment)
        .subscribe(
        data => {
          console.log(data);
          this.closeModal();
          this.toastProvider.presentToast('Assessment created sucessfully.');

        },
        error => {
          console.log(error);
          console.log("In Error");
          this.toastProvider.presentToast('Error while creating assessment.');
        });

        }

  }

  addAnswers(answerList: Answer[], i: number){
    console.log(answerList);
    var modalPage = this.modalCtrl.create('AddAnswersPage', {questionAnswerList: answerList});
    modalPage.onDidDismiss((data: Answer[]) => {
      console.log("On answers dismiss - ");
      console.log(data);
      this.assessment.Quiz[i].Answers = data;
      this.assessment.Quiz[i].AnswerString = [];
      data.forEach(element => {
        this.assessment.Quiz[i].AnswerString.push(element.Code);
      });
      console.log(this.assessment.Quiz[i].AnswerString);
    });
    modalPage.present(); 
  }


}
