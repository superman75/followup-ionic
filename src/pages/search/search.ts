import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionManagerProvider } from '../../providers/session-manager/session-manager';
import { ServiceHelperProvider } from '../../providers/service-helper/service-helper';
import { UserBasicInfo } from '../../providers/modals/modals';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchQuery: string = '';
  items: UserBasicInfo[] = [];
  isPatient: boolean;
  placeholderText:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public sessionManagerProvider: SessionManagerProvider, public serviceProvider: ServiceHelperProvider) {
    this.isPatient = sessionManagerProvider.getPatient();
    if (this.isPatient) {
      this.placeholderText="Doctor";
    }else{
      this.placeholderText="Patient";
    }
    this.initializeItems();
  }
  initializeItems() {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
  getItems(ev: any) {
    // set val to the value of the searchbar
    let val = ev.target.value;

    if (!val || val.trim() == '' || val.trim().length <= 3) 
      {
        return;
      }

    this.serviceProvider.getRequest("api/Assessment/FindAllUsers?searchString=" + val)
    .subscribe(
        (data: UserBasicInfo[]) => {
          this.items = data;     
          this.items.forEach(element => {
            if (element.Photo) {
              element.Photo="data:image/png;base64,"+element.Photo;
            }else{
              element.Photo="";
            }
            
          });
           
        
        },
        error => {
          console.log(error);
        });
    
  }

}
