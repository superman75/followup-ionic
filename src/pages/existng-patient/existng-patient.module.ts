import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExistngPatientPage } from './existng-patient';
import { ComponentsModule } from '../../components/components.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    ExistngPatientPage,
  ],
  imports: [
    ComponentsModule,
    ChartsModule,
    IonicPageModule.forChild(ExistngPatientPage),
  ],
})
export class ExistngPatientPageModule {}
