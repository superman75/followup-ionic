import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PatientResult,Gender,PatientResultView } from '../../providers/modals/modals';
import { ServiceProvider } from './../../providers/Serviceprovider/service';
import {ServiceHelperProvider} from '../../providers/service-helper/service-helper';
import { concat } from 'rxjs/operators/concat';
import {ToastProvider } from '../../providers/toast/toast';
//import { Chart } from 'chart.js';
import { ChartsModule } from 'ng2-charts';
/**
 * Generated class for the ExistngPatientPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-existng-patient',
  templateUrl: 'existng-patient.html',
})
export class ExistngPatientPage {
   showDetail:boolean = false;
  activePatients: PatientResultView[] = [];
  inActivePatients: PatientResultView[] = [];
  allPatient:PatientResult[]=[];
  patientView:PatientResultView;
  url:String ;
  completedAssessmentsCount: number;
  remainingAssessmentCount: number;

  
  // Doughnut
public doughnutChartLabels:string[] = ['Completed', 'Remaining'];
public doughnutChartData:number[]=[]; //= [7, 3];//=[];
public doughnutChartType:string = 'doughnut';


public doughnutChartColor: any[] = [
  { 
    backgroundColor:["#04E6F7", "#E2E6EF"] 
  }];

  constructor(public navCtrl: NavController, public navParams: NavParams,public serviceHelperProvider:ServiceHelperProvider,public toastProvider: ToastProvider) {
    console.log('constructor');
    this.LoadPatients();
  }

  


  LoadPatients() {
    
        this.serviceHelperProvider.getRequest("api/Assessment/GetExistingPatientsResults")
        .subscribe(
            data => {
              this.allPatient= <PatientResult[]>data;
              this.activePatients=[];
              this.inActivePatients=[];
              this.allPatient.forEach(element => {
                this.patientView=<PatientResultView>element;
                this.patientView.ShowResult=false;
                
                
                if(this.patientView.IsActive)
                           this.activePatients.push(this.patientView);
                           else
                         this.inActivePatients.push(this.patientView);
             });
              console.log(this.activePatients);
            },
            error => {
             console.log(error);
            });
  }

  ionViewDidLoad() {
    
  }
  viewPatientDetail(item:any) {
   // this.showDetail = !this.showDetail;
    item.ShowResult= !item.ShowResult;
    this.doughnutChartData=[];
    // completedAssessmentsCount: number;
    // remainingAssessmentCount: number;
    this.doughnutChartData.push(item.CompletedAssessmentsCount);
    this.doughnutChartData.push(item.TotalAssessmentCount-item.CompletedAssessmentsCount);
  }
  viewPatientProfile(item: PatientResult) {
    let temp = [];
    console.log(item);
    this.allPatient.forEach(a => 
    {
      if(a.PatientGuid == item.PatientGuid)
        {
          temp.push(a);
        }
    });
    this.serviceHelperProvider.setSelectedPatientResults(temp);
    this.navCtrl.push('PatientProfilePage',{selectedTab:"patient"});
  }
  updatePatientStatus(item){
   
   this.url='api/Account/MarkPatientActiveInActive?active='+item.IsActive+'&patientGuid='+item.PatientGuid;
   console.log(item);
    let item1:any={"active":item.IsActive,"patientGuid":item.PatientGuid};
    
    this.serviceHelperProvider.PostRequest(this.url,'')
    .subscribe(
        data => {
         console.log(data);
         this.LoadPatients();
         this.toastProvider.presentToast("Patient status changed successfully.");
        },
        error => {
         console.log("In Error");
         this.toastProvider.presentToast("Error while changing patient status, Please try again later.");
        });
    
  }

}
